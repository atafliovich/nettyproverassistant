%nat->nat->int
a=b||a=c||b=c
%(<<a:bool -> a && ⊤>> b) && a && c
%a&&b-->(a||b||c)
%if a then b else c end
%∀ <<a:(bool,a)->a||!a>> && <<a:bool->a||!a>> b && a && <<a:(bool,a)->a||!a>> b
%<<a:bool->a||!a>> b && a
%(b-->!!a)&&(a=!b)
%a&&b&&c
%(!a-->!b)&&(a!=b)||(a&&c-->b&&c)
%(!a-->!b)&&(a=!b)
%a&&!b
%(b&&c)-->(b&&c)
%(a-b)+c+d
%(a-->b)&&a=b
%(b-->a)&&(a=!b)
%A=(A-->Santa) % if this sentence is true, then Santa exists
%a=b||a=c||b=c
%(a=c)&&((a||c)-->b)
%!a-->(a-->b)
%<<a:bool -> a && b>>
