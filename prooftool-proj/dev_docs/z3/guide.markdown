# Netty Z3 Plugin Guide

## Basic Usage (Java)
Create a `Z3Plugin` object. Add Netty expressions as context with the
`addContext` method, and then try to prove something using the `canProve`
method.

If you want to try to prove something else, you need to call `reset` and re-add
the context.

Right now `canProve` always returns `false` if Z3 cannot prove the statement
and does not distinguish between the case in which Z3 proves the statement
false and the case in which it cannot determine either way. This should
probably change. There's also no support for getting at the counterexamples
that Z3 is capable of generating, which is a shame.

## GUI
The "Z3" button in the GUI will attempt to prove the connection between the two
most recent lines. If successful it will change the justification to "proved
with Z3".

## Design
The `Z3Translatable` interface is implemented by Netty objects that can be
translated to Z3 expression objects, currently `Expression` and its various
subclasses. It has one method, `toZ3`. Various methods on the `Z3Plugin` class
are provided for use by classes implementing `Z3Translatable`. These are
`getVar`, `translateLiteral`, and `makeOperation`. These aren't expected to be
used anywhere other than in implementations of `toZ3`.

The `IntermediateVar` and `TypeTranslation` classes take care of translating
between Netty and Z3 types on variables. Netty types are often best implemented
by taking a certain subset of a Z3 type, described by a boolean constraint. An
`IntermediateVar` holds a Z3 `Expr` object representing a variable, together
with the necessary constraint. This is used by `Z3Plugin` to make sure the
constraint is put in the right place, whether the variable is free or
quantified.

`TypeTranslation` (as far as its public interface goes) a static class which is
used to create instances of `IntermediateVar`. Actually various instances exist
which are used to translate particular types like `bool` and `nat`.

## Stuff That Isn't Supported (Yet?)
Rational numbers (Netty's `rat` type) aren't supported. Z3 doesn't have a
rational number type, and while we could treat them as real numbers with a
constraint, the constraint would involve a quantifier which is no good.

Scopes aren't supported and it is unclear how they would be implemented in Z3.
(Quantifiers are implemented in Netty using scopes but are special-cased by the
Z3 plugin.)

Z3 has support for floating point numbers, so they could be implemented without
much difficulty; however, the `Z3Plugin` class would need to be refactored
somewhat. Right now an operator like `+` always translates to the same Z3
operator without considering the types, but in Z3 floating-point operations are
distinguished from regular arithmetic operations.

Lists are not supported. Neither Z3 lists nor Z3 arrays map properly to Netty
lists. Z3 has sets which could likely be used to implement sets and/or bunches
in Netty but this wasn't really explored.
