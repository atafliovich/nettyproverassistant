package z3parser;

import com.microsoft.z3.BoolExpr;

public abstract class Token {

    public int getLbp() {
        return 0;
    }

    public BoolExpr nud(Parser p) {
        throw new RuntimeException("no nud here");
    }

    public BoolExpr led(Parser p, BoolExpr left) {
        throw new RuntimeException("no led here");
    }

}
