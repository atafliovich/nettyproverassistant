package z3parser;

import com.microsoft.z3.BoolExpr;

public class AndToken extends Token {

    public int getLbp() {
        return 11;
    }

    public BoolExpr led(Parser p, BoolExpr left) {
        BoolExpr right = p.expression(11);
        return p.getZ3Context().mkAnd(left, right);
    }

}
