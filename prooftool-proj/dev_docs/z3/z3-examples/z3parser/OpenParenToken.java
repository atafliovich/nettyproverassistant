package z3parser;

import com.microsoft.z3.BoolExpr;

class OpenParenToken extends Token {

    public BoolExpr nud(Parser p) {
        BoolExpr contents = p.expression();
        if (!(p.popToken() instanceof CloseParenToken)) {
            throw new RuntimeException("missing close-paren");
        }
        return contents;
    }

}
