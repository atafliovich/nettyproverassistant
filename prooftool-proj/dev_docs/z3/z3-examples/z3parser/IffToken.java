package z3parser;

import com.microsoft.z3.BoolExpr;

public class IffToken extends Token {

    public int getLbp() {
        return 13;
    }

    public BoolExpr led(Parser p, BoolExpr left) {
        BoolExpr right = p.expression(13);
        return p.getZ3Context().mkIff(left, right);
    }

}
