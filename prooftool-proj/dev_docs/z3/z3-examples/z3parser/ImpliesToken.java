package z3parser;

import com.microsoft.z3.BoolExpr;

public class ImpliesToken extends Token {

    public int getLbp() {
        return 9;
    }

    public BoolExpr led(Parser p, BoolExpr left) {
        BoolExpr right = p.expression(9);
        return p.getZ3Context().mkImplies(left, right);
    }

}
