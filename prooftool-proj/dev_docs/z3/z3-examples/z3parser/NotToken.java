package z3parser;

import com.microsoft.z3.BoolExpr;

public class NotToken extends Token {

    public BoolExpr nud(Parser p) {
        BoolExpr operand = p.expression(12);
        return p.getZ3Context().mkNot(operand);
    }

}
