package z3parser;

import com.microsoft.z3.Context;

public class Demo {

    public static void main(String[] args) {
        Context ctx = new Context();
        for (String arg : args) {
            System.out.println(Parser.parse(ctx, arg));
        }
    }

}
