package z3parser;

import com.microsoft.z3.BoolExpr;

public class VariableToken extends Token {

    private String name;

    public VariableToken(String name) {
        this.name = name;
    }

    public BoolExpr nud(Parser p) {
        return p.getZ3Context().mkBoolConst(name);
    }

}
