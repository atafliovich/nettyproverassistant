package z3parser;

import com.microsoft.z3.Context;
import com.microsoft.z3.BoolExpr;

public class Parser {

    private Context ctx;
    private Token nextToken;
    private TokenStream ts;

    public static BoolExpr parse(Context ctx, String input) {
        TokenStream ts = new TokenStream(input);
        Parser p = new Parser(ctx, ts);
        return p.expression();
    }

    public Parser(Context ctx, TokenStream ts) {
        this.ctx = ctx;
        this.nextToken = ts.next();
        this.ts = ts;
    }

    public BoolExpr expression() {
        return expression(0);
    }

    public BoolExpr expression(int rbp) {
        Token token = popToken();
        BoolExpr left = token.nud(this);
        while (nextToken != null && nextToken.getLbp() > rbp) {
            token = popToken();
            left = token.led(this, left);
        }
        return left;
    }

    public Context getZ3Context() {
        return ctx;
    }

    public Token popToken() {
        Token t = nextToken;
        nextToken = ts.next();
        return t;
    }

}
