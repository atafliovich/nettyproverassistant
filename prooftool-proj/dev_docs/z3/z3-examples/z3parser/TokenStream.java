package z3parser;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class TokenStream {

    private static Pattern pat = Pattern.compile(
        "\\s*((?<op>[&\\|~\\(\\)]|=>|==)|(?<var>\\w+))\\s*");

    private Matcher matcher;

    public TokenStream(String input) {
        matcher = pat.matcher(input);
    }

    public Token next() {
        if (!matcher.find()) return null;

        if (matcher.group("op") != null) {
            switch (matcher.group("op")) {
            case "&":
                return new AndToken();
            case "|":
                return new OrToken();
            case "~":
                return new NotToken();
            case "=>":
                return new ImpliesToken();
            case "==":
                return new IffToken();
            case "(":
                return new OpenParenToken();
            case ")":
                return new CloseParenToken();
            }
        }

        assert matcher.group("var") != null;
        return new VariableToken(matcher.group("var"));
    }

}
