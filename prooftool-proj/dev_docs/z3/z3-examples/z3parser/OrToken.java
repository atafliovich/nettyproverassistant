package z3parser;

import com.microsoft.z3.BoolExpr;

public class OrToken extends Token {

    public int getLbp() {
        return 10;
    }

    public BoolExpr led(Parser p, BoolExpr left) {
        BoolExpr right = p.expression(10);
        return p.getZ3Context().mkOr(left, right);
    }

}
