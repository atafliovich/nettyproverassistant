package z3tryout;

import com.microsoft.z3.*;
import static z3tryout.Main.*;

class ListsTake2 {

    public static void main(String[] args) {
        trivial();
    }

    private static void trivial() {
        ListSort intList = ctx.mkListSort("intList", ctx.getIntSort());

        Expr nil = intList.getNil();
        FuncDecl cons = intList.getConsDecl();
        FuncDecl isNil = intList.getIsNilDecl();

        Expr x = ctx.mkIntConst("x");
        Expr xs = ctx.mkConst("xs", intList);

        BoolExpr bs = (BoolExpr)ctx.mkApp(isNil, ctx.mkApp(cons, x, xs));

        Solver solver = ctx.mkSolver();
        solver.add(bs);
        printResults(solver);
    }

    private static void example2() {
    }

    private static void example3() {
    }

    private static void example4() {
    }

}
