package z3tryout;

import com.microsoft.z3.*;
import static z3tryout.Main.*;

public class Parse {

    public static void main(String[] args) {
        String s =
            "(declare-fun f (Int Int) Int)" +
            "(assert (forall ((x Int))" +
            "                (= (f x x) 0)))" +
            "(assert (exists ((a Int) (b Int))" +
            "                (not (= (f a b) 0))))";
        BoolExpr e = ctx.parseSMTLIB2String(s, null, null, null, null);
        System.out.println(e);

        Solver solver = ctx.mkSolver();
        solver.add(e);
        printResults(solver);
    }

}
