package z3tryout;

// import java.util.Map;
// import java.util.HashMap;
import com.microsoft.z3.*;
import z3parser.Parser;
import static z3tryout.Main.*;

public class Leqv {

    // private static Context ctx;

    public static void main(String[] args) {
        /*
        Map<String, String> config = new HashMap<>();
        config.put("proof", "true");
        ctx = new Context(config);
        */

        BoolExpr x = ctx.mkBoolConst("x");
        BoolExpr y = ctx.mkBoolConst("y");
        BoolExpr z = ctx.mkBoolConst("z");

        BoolExpr[][] leqvs = {

            {ctx.mkImplies(ctx.mkIff(x, ctx.mkNot(y)), z),
             ctx.mkOr(
                ctx.mkAnd(x, y),
                ctx.mkAnd(ctx.mkNot(x), ctx.mkNot(y)),
                z)},

            {Parser.parse(ctx, "(x == ~y) => z"),
             Parser.parse(ctx, "x&y | ~x&~y | z")},

            {ctx.mkImplies(
                ctx.mkImplies(x, ctx.mkNot(y)),
                ctx.mkNot(ctx.mkImplies(x, y))),
             ctx.mkImplies(y, x)},

            {ctx.mkImplies(
                ctx.mkIff(x, ctx.mkNot(y)),
                ctx.mkNot(ctx.mkImplies(x, y))),
             ctx.mkImplies(y, x)},

            {ctx.mkImplies(ctx.mkAnd(x, ctx.mkNot(y)), ctx.mkNot(z)),
             ctx.mkImplies(ctx.mkAnd(x, z), y)},

            {ctx.mkAnd(ctx.mkImplies(x, y), ctx.mkImplies(x, z)),
             ctx.mkImplies(x, ctx.mkAnd(y, z))},

            {ctx.mkAnd(ctx.mkImplies(y, x), ctx.mkImplies(z, x)),
             ctx.mkImplies(ctx.mkAnd(y, z), x)},

            {Parser.parse(ctx, "a | b | c | d | e | f | g | h | i | j | k | l"),
             Parser.parse(ctx, "~(~a&~b&~c&~d&~e&~f&~g&~h&~i&~j&~k&~l)")},

            {Parser.parse(ctx, "a&b => c | d | e"),
             Parser.parse(ctx, "~a | ~b | c | d | e")},

            {Parser.parse(ctx, "a | b | c | d | e | f | g | h | i | j"),
             Parser.parse(ctx, "a | ~(~b | ~c) | d | e | f | g | h | i | j")},

        };

        for (BoolExpr[] e : leqvs) {
            System.out.println(e[0]);
            System.out.println(e[1]);
            proveLeqv(e[0], e[1]);
            System.out.println("---");
        }
    }

    public static void proveLeqv(BoolExpr lhs, BoolExpr rhs) {
        // a LEQV b <=> a XOR b is unsatisfiable
        Solver solver = ctx.mkSolver();

        solver.add(ctx.mkXor(lhs, rhs));
        switch (solver.check()) {
        case SATISFIABLE:
            System.out.println("NOT LEQV");
            System.out.println(solver.getModel());
            break;
        case UNSATISFIABLE:
            System.out.println("LEQV");
            break;
        case UNKNOWN:
            System.out.println("UNKNOWN");
        }
    }

}
