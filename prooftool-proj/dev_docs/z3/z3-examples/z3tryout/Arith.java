package z3tryout;

import com.microsoft.z3.*;
import static z3tryout.Main.*;

public class Arith {

    public static void main(String[] args) {
        binomial();
    }

    public static void proveEquiv(Expr lhs, Expr rhs) {
        Solver solver = ctx.mkSolver();

        solver.add(ctx.mkNot(ctx.mkEq(lhs, rhs)));
        switch (solver.check()) {
        case SATISFIABLE:
            System.out.println("NOT EQUIV");
            System.out.println(solver.getModel());
            break;
        case UNSATISFIABLE:
            System.out.println("EQUIV");
            break;
        case UNKNOWN:
            System.out.println("UNKNOWN");
        }
    }

    private static void binomial() {
        RealExpr x = ctx.mkRealConst("x");

        ArithExpr a = ctx.mkAdd(
            ctx.mkPower(x, ctx.mkInt(5)),
            ctx.mkMul(ctx.mkInt(5), ctx.mkPower(x, ctx.mkInt(4))),
            ctx.mkMul(ctx.mkInt(10), ctx.mkPower(x, ctx.mkInt(3))),
            ctx.mkMul(ctx.mkInt(10), ctx.mkPower(x, ctx.mkInt(2))),
            ctx.mkMul(ctx.mkInt(5), x),
            ctx.mkInt(1));

        ArithExpr b = ctx.mkPower(ctx.mkAdd(x, ctx.mkInt(1)), ctx.mkInt(5));

        System.out.println(a);
        System.out.println(b);

        proveEquiv(a, b);
    }

    private static void someOtherThing() {
        RealExpr x = ctx.mkRealConst("x");
    }

}
