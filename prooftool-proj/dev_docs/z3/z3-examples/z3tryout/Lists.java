package z3tryout;

import com.microsoft.z3.*;
import static z3tryout.Main.*;

class Lists {

    public static void main(String[] args) {
        binSearch();
    }

    public static void binSearch() {
        Sort T = ctx.mkUninterpretedSort("T");

        FuncDecl le = ctx.mkFuncDecl("le", new Sort[] {T, T}, ctx.getBoolSort());

        Expr x = ctx.mkConst("x", T);
        Expr y = ctx.mkConst("y", T);
        Expr z = ctx.mkConst("z", T);

        // le is a total order on T.
        Quantifier reflexive = ctx.mkForall(
            new Expr[] {x},
            (BoolExpr)ctx.mkApp(le, x, x),
            0, null, null, null, null);
        Quantifier antisym = ctx.mkForall(
            new Expr[] {x, y},
            ctx.mkImplies(
                (BoolExpr)ctx.mkApp(le, x, y),
                ctx.mkNot((BoolExpr)ctx.mkApp(le, x, y))),
            0, null, null, null, null);
        Quantifier transitive = ctx.mkForall(
            new Expr[] {x, y, z},
            ctx.mkImplies(
                ctx.mkAnd(
                    (BoolExpr)ctx.mkApp(le, x, y),
                    (BoolExpr)ctx.mkApp(le, y, z)),
                (BoolExpr)ctx.mkApp(le, x, z)),
            0, null, null, null, null);
        Quantifier total = ctx.mkForall(
            new Expr[] {x, y},
            ctx.mkOr(
                (BoolExpr)ctx.mkApp(le, x, y),
                (BoolExpr)ctx.mkApp(le, y, z)),
            0, null, null, null, null);

        System.out.println(reflexive);
        System.out.println(antisym);
        System.out.println(transitive);
        System.out.println(total);
    }

    public static void isSorted() {
        int[] a1 = {1, 2, 3, 5, 5, 12, 89};
        int[] a2 = {1, 2, 4, 0, 100, 101, 102};

        Sort z3Int = ctx.getIntSort();
        ArrayExpr za1 = ctx.mkArrayConst("za1", z3Int, z3Int);
        ArrayExpr za2 = ctx.mkArrayConst("za2", z3Int, z3Int);

        for (int i = 0; i < 7; ++i) {
            za1 = ctx.mkStore(za1, ctx.mkInt(i), ctx.mkInt(a1[i]));
            za2 = ctx.mkStore(za2, ctx.mkInt(i), ctx.mkInt(a2[i]));
        }

        System.out.println(za1);
        System.out.println(za2);
    }

}
