package z3tryout;

import com.microsoft.z3.*;
import z3parser.Parser;
import static z3tryout.Main.*;

public class Ass {

    public static void main(String[] args) {
        IntExpr x = ctx.mkIntConst("x");
        BoolExpr a = ctx.mkBoolConst("A1");

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkLt(x, ctx.mkInt(0)));
        solver.add(ctx.mkImplies(a, ctx.mkEq(x, ctx.mkInt(1))));

        Status s = solver.check(a);
        System.out.println(s);
        if (s == Status.SATISFIABLE) {
            System.out.println(solver.getModel());
        }
        else if (s == Status.UNSATISFIABLE) {
            for (Expr e: solver.getUnsatCore()) System.out.println(e);
        }
    }

}
