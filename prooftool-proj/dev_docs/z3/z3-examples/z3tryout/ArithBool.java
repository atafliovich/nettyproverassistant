package z3tryout;

import com.microsoft.z3.*;
import static z3tryout.Main.*;

class ArithBool {

    public static  void main(String[] args) {
        example1();
        example2();
        example3();
        example4();
        sqrt2();
        example5();
        example6();
    }

    private static void example1() {
        ArithExpr x = ctx.mkRealConst("x");
        ArithExpr y = ctx.mkRealConst("y");

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkEq(x, ctx.mkReal(1, 3)));  // x = 1/3
        solver.add(ctx.mkEq(y, ctx.mkReal(2)));     // y = 2
        solver.add(
            ctx.mkNot(
                ctx.mkEq(
                    ctx.mkMul(x, y),
                    ctx.mkReal(2, 3))));            // xy != 2/3

        printResults(solver);
    }

    private static void example2() {
        ArithExpr x = ctx.mkRealConst("x");
        ArithExpr y = ctx.mkRealConst("y");

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkEq(x, ctx.mkReal(1)));                 // x = 1
        solver.add(ctx.mkLt(y, ctx.mkMul(x, ctx.mkReal(42))));  // y < 42x
        solver.add(ctx.mkGe(y, ctx.mkReal(42)));                // y >= 42

        printResults(solver);
    }

    private static void example3() {
        ArithExpr x = ctx.mkRealConst("x");
        ArithExpr y = ctx.mkRealConst("y");

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkGt(x, ctx.mkReal(1)));     // x > 1
        solver.add(ctx.mkLt(y, ctx.mkReal(10)));    // y < 10
        solver.add(ctx.mkLe(
                       ctx.mkDiv(x, y),
                       ctx.mkReal(1, 10)));         // x/y <= 1/10

        // This one is satisfiable for negative y.
        printResults(solver);
    }

    private static void example4() {
        ArithExpr x = ctx.mkRealConst("x");
        ArithExpr y = ctx.mkRealConst("y");

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkGt(x, ctx.mkReal(1)));     // x > 1
        solver.add(ctx.mkGt(y, ctx.mkReal(0)));     // y > 0
        solver.add(ctx.mkLt(y, ctx.mkReal(10)));    // y < 10
        solver.add(ctx.mkLe(
                       ctx.mkDiv(x, y),
                       ctx.mkReal(1, 10)));         // x/y <= 1/10

        printResults(solver);
    }

    // Does not work.
    private static void sqrt2() {
        ArithExpr a = ctx.mkInt2Real(ctx.mkIntConst("a"));
        ArithExpr b = ctx.mkInt2Real(ctx.mkIntConst("b"));

        // a^2 / b^2 = 2
        BoolExpr bs = ctx.mkEq(
            ctx.mkDiv(ctx.mkMul(a, a), ctx.mkMul(b, b)),
            ctx.mkReal(2));

        Solver solver = ctx.mkSolver();
        // Without specifying that x and y are nonzero, Z3 considers
        // 0/0 = 2 a valid solution.
        solver.add(ctx.mkGt(a, ctx.mkReal(0)));
        solver.add(ctx.mkLt(b, ctx.mkReal(0)));
        solver.add(bs);
 
        printResults(solver);
    }

    // Does not work.
    private static void example5() {
        ArithExpr x = ctx.mkRealConst("x");

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkEq(
                       ctx.mkPower(
                           ctx.mkReal(3),
                           x),
                       ctx.mkReal(2)));             // 3^x = 2
        solver.add(ctx.mkGe(x, ctx.mkReal(1)));     // x >= 1

        printResults(solver);
    }

    private static void example6() {
        ArithExpr x = ctx.mkRealConst("x");
        ArithExpr y = ctx.mkRealConst("y");
        ArithExpr zero = ctx.mkReal(0);

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkForall(
                       new Expr[] {y},
                       ctx.mkEq(ctx.mkMul(x, y), zero),
                       0, null, null, null, null));
        solver.add(ctx.mkNot(ctx.mkEq(x, zero)));

        printResults(solver);
    }

}
