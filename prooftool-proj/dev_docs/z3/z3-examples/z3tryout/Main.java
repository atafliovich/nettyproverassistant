package z3tryout;

import com.microsoft.z3.*;

public class Main {

    static Context ctx = new Context();

    public static void main(String[] args) {
        // polynomialConstraint();
        // eightRooks();
        // quantifiers();
        // injective();
        // additionExists();
        groupsExist(4);
    }

    static void printResults(Solver solver) {
        Status s = solver.check();
        System.out.println(s);
        if (s == Status.SATISFIABLE) {
            System.out.println(solver.getModel());
        }
    }

    private static void polynomialConstraint() {
        RealExpr x = ctx.mkRealConst("x");
        RealExpr y = ctx.mkRealConst("y");

        BoolExpr constraint = ctx.mkGt(
            ctx.mkAdd(
                ctx.mkPower(x, ctx.mkInt(2)),
                ctx.mkPower(y, ctx.mkInt(2))),
            ctx.mkInt(3));
        System.out.println(constraint);

        Solver solver = ctx.mkSolver();
        solver.add(constraint);
        printResults(solver);
    }

    private static void eightRooks() {
        IntExpr[] r = new IntExpr[8];
        for (int i = 0; i < 8; ++i) {
            String name = String.format("r[%d]", i);
            r[i] = ctx.mkIntConst(name);
        }

        Solver solver = ctx.mkSolver();

        // Eight columns.
        for (int i = 0; i < 8; ++i) {
            solver.add(ctx.mkGe(r[i], ctx.mkInt(0)));
            solver.add(ctx.mkLt(r[i], ctx.mkInt(8)));
        }

        // Only one rook per column.
        solver.add(ctx.mkDistinct(r));

        printResults(solver);
    }

    private static void quantifiers() {
        Sort[] domain = {ctx.getIntSort(), ctx.getIntSort()};
        FuncDecl f = ctx.mkFuncDecl("f", domain, ctx.getIntSort());

        IntExpr a = ctx.mkIntConst("a");
        IntExpr b = ctx.mkIntConst("b");
        IntExpr x = ctx.mkIntConst("x");

        BoolExpr body = ctx.mkEq(
            ctx.mkApp(f, x, x),
            ctx.mkInt(0));
        Quantifier alt = ctx.mkForall(
            new Expr[] {x},
            body,
            0,
            null,
            null,
            null,
            null);
        System.out.println(alt);

        body = ctx.mkNot(
            ctx.mkEq(
                ctx.mkApp(f, a, b),
                ctx.mkInt(0)));
        Quantifier nonzero = ctx.mkExists(
            new Expr[] {a, b},
            body,
            0,
            null,
            null,
            null,
            null);
        System.out.println(nonzero);

        Solver solver = ctx.mkSolver();
        solver.add(alt, nonzero);
        printResults(solver);
    }

    // Z3 chokes on this one.
    private static void injective() {
        FuncDecl f = ctx.mkFuncDecl(
            "f",
            new Sort[] {ctx.getIntSort()},
            ctx.getIntSort());
        IntExpr x = ctx.mkIntConst("x");
        IntExpr y = ctx.mkIntConst("y");

        BoolExpr body = ctx.mkImplies(
            ctx.mkEq(
                ctx.mkApp(f, x),
                ctx.mkApp(f, y)),
            ctx.mkEq(x, y));
        Quantifier inj = ctx.mkForall(
            new Expr[] {x, y},
            body,
            0,
            null,
            null,
            null,
            null);
        System.out.println(inj);
  
        Solver solver = ctx.mkSolver();
        solver.add(inj);
        printResults(solver);
    }

    private static void additionExists() {
        FuncDecl f = ctx.mkFuncDecl(
            "f",
            new Sort[] {ctx.getIntSort(), ctx.getIntSort()},
            ctx.getIntSort());
        IntExpr x = ctx.mkIntConst("x");
        IntExpr y = ctx.mkIntConst("y");

        BoolExpr body = ctx.mkEq(
            ctx.mkApp(f, x, y),
            ctx.mkAdd(x, y));
        Quantifier q = ctx.mkForall(
            new Expr[] {x, y},
            body,
            0,
            null,
            null,
            null,
            null);
        System.out.println(q);

        Solver solver = ctx.mkSolver();
        solver.add(q);
        printResults(solver);
    }

    public static void groupsExist(int order) {
        String[] names = new String[order];
        for (int i = 0; i < order; ++i) {
            names[i] = String.format("g%d", i);
        }

        Sort G = ctx.mkEnumSort("G", names);
        FuncDecl dot = ctx.mkFuncDecl(".", new Sort[] {G, G}, G);
        FuncDecl inverse = ctx.mkFuncDecl("inverse", new Sort[] {G}, G);

        Expr a = ctx.mkConst("a", G);
        Expr b = ctx.mkConst("b", G);
        Expr c = ctx.mkConst("c", G);
        Expr e = ctx.mkConst("e", G);

        // Associativity
        BoolExpr body = ctx.mkEq(
            ctx.mkApp(dot, ctx.mkApp(dot, a, b), c),
            ctx.mkApp(dot, a, ctx.mkApp(dot, b, c)));
        Quantifier assoc = ctx.mkForall(
            new Expr[] {a, b, c},
            body,
            0,
            null,
            null,
            null,
            null);
        System.out.println(assoc);

        // Identity
        body = ctx.mkAnd(
            ctx.mkEq(ctx.mkApp(dot, a, e), a),
            ctx.mkEq(ctx.mkApp(dot, e, a), a));
        Quantifier id = ctx.mkForall(
            new Expr[] {a},
            body,
            0,
            null,
            null,
            null,
            null);
        System.out.println(id);

        // Invertibility
        body = ctx.mkAnd(
            ctx.mkEq(ctx.mkApp(dot, a, ctx.mkApp(inverse, a)), e),
            ctx.mkEq(ctx.mkApp(dot, ctx.mkApp(inverse, a), a), e));
        Quantifier inv = ctx.mkForall(
            new Expr[] {a},
            body,
            0,
            null,
            null,
            null,
            null);
        System.out.println(inv);

        // Use quantifier elimination so this doesn't take a decade.
        Tactic tac = ctx.andThen(ctx.mkTactic("qe"), ctx.mkTactic("smt"));
        Solver solver = ctx.mkSolver(tac);
        solver.add(assoc, id, inv);
        printResults(solver);
    }

}
