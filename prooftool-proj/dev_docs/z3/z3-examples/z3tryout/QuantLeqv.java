package z3tryout;

import com.microsoft.z3.*;
import z3parser.Parser;
import static z3tryout.Main.*;

public class QuantLeqv {

    public static void main(String[] args) {
        deMorgan();
        anotherOne();
        yetAnotherOne();
    }

    private static void deMorgan() {
        Sort T = ctx.mkUninterpretedSort("T");
        Expr x = ctx.mkConst("x", T);
        FuncDecl p = ctx.mkFuncDecl("p", new Sort[] {T}, ctx.getBoolSort());

        Quantifier q = ctx.mkForall(
            new Expr[] {x},
            ctx.mkApp(p, x),
            0,
            null,
            null,
            null,
            null);

        Quantifier r = ctx.mkExists(
            new Expr[] {x},
            ctx.mkNot((BoolExpr)ctx.mkApp(p, x)),
            0,
            null,
            null,
            null,
            null);

        Leqv.proveLeqv(q, ctx.mkNot(r));
        Leqv.proveLeqv(ctx.mkNot(q), (r));
    }

    private static void anotherOne() {
        Sort T = ctx.mkUninterpretedSort("T");
        Expr x = ctx.mkConst("x", T);
        FuncDecl p1 = ctx.mkFuncDecl("p1", new Sort[] {T}, ctx.getBoolSort());
        FuncDecl p2 = ctx.mkFuncDecl("p2", new Sort[] {T}, ctx.getBoolSort());
        FuncDecl p3 = ctx.mkFuncDecl("p3", new Sort[] {T}, ctx.getBoolSort());
        
        BoolExpr a = (BoolExpr)ctx.mkApp(p1, x);
        BoolExpr b = (BoolExpr)ctx.mkApp(p2, x);
        BoolExpr c = (BoolExpr)ctx.mkApp(p3, x);

        Quantifier q = ctx.mkForall(
            new Expr[] {x},
            ctx.mkImplies(ctx.mkIff(a, ctx.mkNot(b)), c),
            0,
            null,
            null,
            null,
            null);

        Quantifier r = ctx.mkForall(
            new Expr[] {x},
             ctx.mkOr(
                ctx.mkAnd(a, b),
                ctx.mkAnd(ctx.mkNot(a), ctx.mkNot(b)),
                c),
            0,
            null,
            null,
            null,
            null);

        System.out.println(q);
        System.out.println(r);

        Leqv.proveLeqv(q, r);
    }

    private static void yetAnotherOne() {
        Sort T = ctx.mkUninterpretedSort("T");
        Expr x = ctx.mkConst("x", T);
        FuncDecl p1 = ctx.mkFuncDecl("p1", new Sort[] {T}, ctx.getBoolSort());
        FuncDecl p2 = ctx.mkFuncDecl("p2", new Sort[] {T}, ctx.getBoolSort());
        
        Quantifier q = ctx.mkForall(
            new Expr[] {x},
            ctx.mkImplies(
                (BoolExpr)ctx.mkApp(p1, x),
                (BoolExpr)ctx.mkApp(p2, x)),
            0,
            null,
            null,
            null,
            null);

        BoolExpr r = ctx.mkImplies(
            ctx.mkForall(
                new Expr[] {x},
                (BoolExpr)ctx.mkApp(p1, x),
                0,
                null,
                null,
                null,
                null),
            ctx.mkExists(
                new Expr[] {x},
                (BoolExpr)ctx.mkApp(p2, x),
                0,
                null,
                null,
                null,
                null));

        System.out.println(q);
        System.out.println(r);

        Leqv.proveLeqv(q, r);
    }

}
