package z3tryout;

import com.microsoft.z3.*;
import static z3tryout.Main.*;

public class WhatIf {

    public static void main(String[] args) {
        BoolExpr x1 = ctx.mkBoolConst("x");
        IntExpr x2 = ctx.mkIntConst("x");
        IntExpr x3 = ctx.mkIntConst("x");

        Solver solver = ctx.mkSolver();
        solver.add(ctx.mkEq(x1, ctx.mkTrue()));
        solver.add(ctx.mkEq(x2, ctx.mkInt(1)));
        solver.add(ctx.mkEq(x3, ctx.mkInt(2)));

        printResults(solver);
    }

}
