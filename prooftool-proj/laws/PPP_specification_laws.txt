%Written by David Szeto
%This is a law file for Netty.
%Records the specification and program laws as specified by Eric Hehner's aPToP, chapter 11.4.10
%Modified for Predicative Probabilistic Programming by Eric C.R. Hehner

%ok = x′=x ∧ y′=y ∧ ...
%x:= e = x′=e ∧ y′=y ∧ ...
%P. Q = ∃x′′, y′′, ...· 〈x′, y′, ...→P〉 x′′ y′′ ... ∧ 〈x, y, ...→Q〉 x′′ y′′ ...
%P||Q = ∃tP, tQ· 〈t′→P〉tP ∧ 〈t′→Q〉tQ ∧ t′ = max tP tQ

∀b,P,Q:all·if b then P else Q end = (b ∧ P ∨ ¬b ∧ Q) NAMED "If-Then-Else-End"

%var x: T· P = ∃x, x′: T· P
%frame x· P = P ∧ y′=y ∧ ...
%while b do P = t′≥t ∧ (if b then (P. t:= t+1. while b do P) else ok)
%∀σ, σ′· (if b then (P. W) else ok ⇐ W) ⇒ ∀σ, σ′· (while b do P ⇐ W)
%(Fmn ⇐ m=n ∧ ok) ∧ (Fik ⇐ m≤i<j<k≤n ∧ (Fij. Fjk))
%⇒
%(Fmn ⇐ for i:= m;..n do m≤i<n ⇒ Fi(i+1))
%Im⇒I′n ⇐ for i:= m;..n do m≤i<n ∧ Ii ⇒ I′(i+1)
%wait until w = t:= max t w
%assert b = if b then ok else (print "error". wait until ∞)
%ensure b = b ∧ ok
%P. (P result e)=e but do not double-prime or substitute in (P result e)
%c? = r:= r+1
%c = Mc rc–1
%c! e = Mc wc = e ∧ Tc wc = t ∧ (wc:= wc+1)
%√c = Tc rc + (transit time) ≤ t
%ivar x: T· S = ∃x: time→T· S
%chan c: T· P = ∃Mc: ∞*T· ∃Tc: ∞*xreal· var rc , w

∀P:all·(ok. P) = (P. ok) NAMED "Identity"
∀P:all·(ok. P) = P NAMED "Identity"
∀P:all·(P. ok) = P NAMED "Identity"
∀P,Q,R:all· (P. (Q. R)) = ((P. Q). R) NAMED "Associativity"
∀P,b:all· if b then P else P end = P NAMED "Idempotence"
∀Q,P,b:all· if ¬b then Q else P end = if b then P else Q end NAMED "Case Reversal"
∀P,b:all· (if b then b ⇒ P else ¬b end ⇒ P)  = P NAMED "Case Creation"
∀P,Q,R,S:all· ((P. R) ∨ (P. S) ∨ (Q. R) ∨ (Q. S)) = (P∨Q. R∨S) NAMED "Distributivity"
∀P,Q,R,S:all· ((P. R) + (P. S) + (Q. R) + (Q. S)) = (P+Q. R+S) NAMED "Distributivity"
∀P,Q,R,b:all· if b then (P. R) else (Q. R) end = (if b then P else Q end. R) NAMED "Distributivity (unprimed b)"

∀P:all·(ok || P) = (P || ok) NAMED "Identity"
∀P:all·(ok || P) = P NAMED "Identity"
∀P:all·(P || ok) = P NAMED "Identity"
∀P,Q:all· (P || Q) = (Q || P) NAMED "Symmetry"
∀P,Q,R:all· (P || (Q || R)) = ((P || Q) || R) NAMED "Associativity"
∀P,Q,R:all· ((P || Q) ∨ (P || R)) = (P || Q∨R) NAMED "Distributivity"
∀P,Q,R,b:all· if b then (P || Q) else (P || R) end = (P || if b then Q else R end) NAMED "Distributivity"
∀P,Q,R,S,b:all· (if b then P else R end || if b then Q else S end) = if b then (P||Q) else (R||S) end NAMED "Distributivity"

∀b,e,f:all· ∀x:all· if b then x:= e else x:= f end = (x:= if b then e else f end) NAMED "Functional-Imperative"

% These are some useful unicode symbols of operators
% binary: ⊤ ⊥ ¬ ∧ ∨ ⇐ ⇒ = ≠
% numeric: + − × ∕ ^ ≤ < ≥ >
% bunch: , ‘ ,.. : ¢ ⚡
% set: # ~ { } ∈ ⊆ ⊂ ⋃ ⋂
% strings: ; ;.. ∗ " " _ ⊲ ⊳ $ [ ]
% extra: ♮ ⟷
% functions: ⟨ → ⟩ ↦ | ∆
% quantifiers: ∀ ∃ ∑ § ∏ ·  
% programming: x′ := ok . ||
