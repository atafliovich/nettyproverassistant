/**
 * 
 */
package tester;

/**
 * @author ben
 *
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
   TwoOperatorParsingTests.class,
   TestRunner.class,
   //add more testing classes here
})
public class TestSuite {   
} 
