package tester;

import java.io.File;
import java.util.ArrayList;
//import java.util.Collection;
import java.nio.file.Files;
import java.nio.file.Paths;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import prooftool.util.Main;

/**
 * 
 * @author ben
 *
 */
@RunWith(Parameterized.class)
public class TestRunner extends TestCase{
	
	private String expected;
	private String actual;
	
	public TestRunner(String raw, String expected) throws Exception {
		this.expected = expected;
		this.actual = Main.parseSingle(raw).toString();
	}
	
	
	@Parameters
	public static ArrayList<String[]> getTests() throws Exception {
		ArrayList<String> raw_lines = new ArrayList<String>();
		ArrayList<String []> tests = new ArrayList<String []>();
		//reading all the files in the directory
		File folder = new File("dev_docs/junittests");
		String [] files = folder.list();
		
		for (String file : files) {
			raw_lines.addAll(Files.readAllLines(Paths.get((folder.getAbsolutePath() + "/" + file))));
		}
		
		for (int i=0; i+1 < raw_lines.size(); i+=2) {
			tests.add(new String[]{raw_lines.get(i), raw_lines.get(i+1)});
		}
		
		return tests;
	}
	
	@Test
	public void test_runner() throws Exception {
		assertEquals(this.expected, this.actual);
	}
}
