/**
 * 
 */
package tester;

import org.junit.Test;
import prooftool.backend.laws.Expression;
import prooftool.util.Main;
import junit.framework.*;

/**
 * @author ben
 *
 */
public class TwoOperatorParsingTests extends TestCase{
	
	@Test
	public void test_and_or_precedence() throws Exception {
		Expression result = Main.parseSingle("a ∧ b ∨ c");
		assertEquals("(a∧b)∨c", result.toString());
	}

	@Test
	public void test_or_left_associative() throws Exception{
		Expression result = Main.parseSingle("a ∨ b ∨ c");
		assertEquals("(a∨b)∨c", result.toString());
	}

}
