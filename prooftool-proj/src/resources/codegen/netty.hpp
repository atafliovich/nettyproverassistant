#include <vector>
#include <deque>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <cassert>
#include <cstdlib>

namespace netty
{
    template <typename T>
    class list;
    
    template <typename T>
    class string
    {
    public:
        string() {}
        // Anything is a one-item string.
        string(const T& i) : items(1, i) {}
        
        T subscript(int index) const
        {
            assert(0 <= index && index < items.size());
            return items[index];
        }
        
        template <typename U>
        string<decltype(subscript(U()))> subscript(const string<U>& s) 
        {
            string<decltype(subscript(U()))> result;
            result.reserve(s.length());
            for (const U& u : s)
                result.push_back(subscript(u));
            return result;
        }
        
        template <typename U>
        list<decltype(subscript(U()))> subscript(const list<U>& L)
        {
            return make_list(subscript(L.contents()));
        }
        
        size_t length() const
        {
            return items.size();
        }
        
        T back() const
        {
            assert(!items.empty());
            return items.back();
        }
        
        string<T> chop_back() const
        {
            assert(!items.empty());
            string<T> result(*this);
            result.items.pop_back();
            return result;
        }
        
        string<T> concat(const string<T>& other) const
        {
            string<T> result;
            result.items.reserve(length() + other.length());
            result.items.insert(result.items.end(), items.begin(), items.end());
            result.items.insert(result.items.end(), other.items.begin(), other.items.end());
            return result;
        }
        
        friend string operator*(int n, const string& s)
        {
            assert(0 <= n);
            string<T> result;
            result.items.reserve(s.length() * n);
            for (int i = 0; i < n; ++i)
                result.items.insert(result.items.end(), s.items.begin(), s.items.end());
            return result;
        }
        
        string replace(int index, const T& t)const
        {
            assert(0 <= index && index < items.size());
            string result(*this);
            result.items[index] = t;
            return result;
        }
        
        bool operator==(const string& other) const
        {
            return items == other.items;
        }
        
        bool operator<(const string& other) const
        {
            return items < other.items;
        }
        
        bool operator>(const string& other) const
        {
            return items > other.items;
        }
        
        bool operator<=(const string& other) const
        {
            return items <= other.items;
        }
        
        bool operator>=(const string& other) const
        {
            return items >= other.items;
        }
        
        bool operator!=(const string& other) const
        {
            return items != other.items;
        }
    private:
        std::vector<T> items;
    };
    
    struct nil_t
    {
        template <typename T>
        operator string<T>() const
        {
            return string<T>();
        }
    };
    
    nil_t nil;
    
    template <typename T>
    class list
    {
    private:
        string<T> contents_;
    public:
        list() {}
        list(const string<T>& s) : contents_(s) {}

        template <typename U>
        auto operator()(const U& u) -> decltype(contents_.subscript(u))
        {
            return contents_.subscript(u);
        }
        
        size_t length() const
        {
            return contents_.length();
        }
        
        string<T> contents() const
        {
            return contents_;
        }
        
        list operator+(const list& s) const
        {
            return list(contents_.concat(s.contents_));
        }
        
        list replace(int index, const T& e)
        {
            return list(contents_.replace(index, e));
        }
        
//        const list& at(nil_t) const
//        {
//            return *this;
//        }
//        
//        T at(int index) const
//        {
//            return contents_.subscript(index);
//        }
//        
//        // Problem: what use as the return type? The type depends on the *value* of the pointer,
//        // (the number of elements), not just its type.
//        auto at(const string<int>& pointer) const
//        {
//            if (pointer.length() == 0)
//                return at(nil);
//            else if (pointer.length() == 1)
//                return at(pointer.subscript(0));
//            else
//                return at(pointer.chop_back()).at(pointer.subscript(pointer.length() - 1));
//        }
        
        bool operator==(const list& other) const
        {
            return contents_ == other.contents_;
        }
        
        bool operator<(const list& other) const
        {
            return contents_ < other.contents_;
        }
        
        bool operator>(const list& other) const
        {
            return contents_ > other.contents_;
        }
        
        bool operator<=(const list& other) const
        {
            return contents_ <= other.contents_;
        }
        
        bool operator>=(const list& other) const
        {
            return contents_ >= other.contents_;
        }
        
        bool operator!=(const list& other) const
        {
            return contents_ != other.contents_;
        }
    };

    // Create a list from a string
    template <typename T>
    list<T> make_list(const string<T>& s)
    {
        return list<T>(s);
    }
    
    // Concatenate two strings
    template <typename T>
    string<T> make_string(const T& t)
    {
        return string<T>(t);
    }
    template <typename T>
    string<T> make_string(const string<T>& s)
    {
        return s;
    }
    template <typename T, typename U>
    auto make_string(const T& a, const U& b) -> decltype(make_string(a).concat(make_string(b)))
    {
        return make_string(a).concat(make_string(b));
    }
    
    // Built-in functions
    
    auto div = [](int x) { return [x](int y) { return x / y; }; };

    template <typename T>
    void print(const T& t)
    {
        std::cout << t;
    }
    
    template <typename T>
    void println(const T& t)
    {
        std::cout << t << '\n';
    }
    
    template <typename T>
    struct channel_impl
    {
        virtual ~channel_impl() = 0;
        virtual T get() const
        {
            throw new std::logic_error("This channel does not support reading");
        }
        virtual void input()
        {
            throw new std::logic_error("This channel does not support reading");
        }
        virtual void signal()
        {
            throw new std::logic_error("This channel does not support signaling");
        }
        virtual void output(const T& e)
        {
            throw new std::logic_error("This channel does not support writing");
        }
        virtual bool check()
        {
            throw new std::logic_error("This channel does not support checking");
        }
    };
    
    template <typename T>
    channel_impl<T>::~channel_impl() {}
    
    template <typename T>
    struct buffered_channel_impl : public channel_impl<T>
    {
    public:
        buffered_channel_impl() : have_current_message(false) {}
        
        T get() const
        {
            assert(have_current_message);
            return current_message;
        }
        
        void input()
        {
            assert(!unread_messages.empty());
            current_message = unread_messages.front();
            unread_messages.pop_front();
            have_current_message = true;
        }
        
        void signal()
        {
            // Quick and dirty. More properly, we should store a variant<message, signal> in the deque.
            // TODO: synchronization
            unread_messages.push_back(T());
        }
        
        void output(const T& e)
        {
            // TODO: synchronization
            unread_messages.push_back(e);
        }
        
        bool check()
        {
            return !unread_messages.empty();
        }
    private:
        std::deque<T> unread_messages;
        T current_message;
        bool have_current_message;
    };
    
    template <typename T>
    struct channel
    {
    public:
        channel(channel_impl<T>* impl = new buffered_channel_impl<T>()) : impl(impl) {}
        
        T get() const { return impl->get(); }  
        void input() { impl->input(); }
        void signal() { impl->signal(); }
        void output(const T& e) { impl->output(e); }
        bool check() { return impl->check(); }
    private:
        std::unique_ptr<channel_impl<T>> impl;
    };
    
    struct input_channel_impl : public channel_impl<char>
    {
    public:
        input_channel_impl(std::istream& is) : is(is), current() {}
        
        char get() const 
        { 
            return current;
        }
        void input()
        {
            current = is.get();
        }
    private:
        std::istream& is;
        char current;
    };
    
    struct output_channel_impl : public channel_impl<char>
    {
    public:
        output_channel_impl(std::ostream& os) : os(os) {}
        
        void output(const char& e)
        {
            os.put(e);
        }
    private:
        std::ostream& os;
    };
    
    channel<char> stdin(new input_channel_impl(std::cin));
    channel<char> stdout(new output_channel_impl(std::cout));
    channel<char> stderr(new output_channel_impl(std::cerr));
}
