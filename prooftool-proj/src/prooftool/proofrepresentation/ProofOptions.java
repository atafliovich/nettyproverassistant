package prooftool.proofrepresentation;

import prooftool.proofrepresentation.ProofRepresentationBits.MonotonicContext;
import prooftool.proofrepresentation.ProofRepresentationBits.ProofSubtype;

/**
 * This object holds the proof properties.
 * 
 * @author maghari1
 *
 */
public class ProofOptions implements Cloneable {

	// These fields need to be saved
	private MonotonicContext mono;
	private ProofSubtype type;

	private boolean hidden;
	
	public ProofOptions(MonotonicContext mono, ProofSubtype type, boolean isHidden) {
		this.mono = mono;
		this.type = type;
		this.hidden = isHidden;
	}
	
	@Override
	public ProofOptions clone() {
		return new ProofOptions(this.mono, this.type, this.hidden);
	}
	
	public MonotonicContext getMonotonicContext() {
		return this.mono;
	}
	
	public ProofSubtype getProofSubtype() {
		return this.type;
	}
	
	public boolean isHidden() {
		return this.hidden;
	}
	
	public void setMonotonicContext(MonotonicContext newMono) {
		this.mono = newMono;
	}
	
	public void setProofSubtype(ProofSubtype newType) {
		this.type = newType;
	}
	
	public void setHidden(boolean newHidden) {
		this.hidden = newHidden;
	}
	
}
