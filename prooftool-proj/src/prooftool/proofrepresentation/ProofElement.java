package prooftool.proofrepresentation;

import java.awt.Rectangle;

import javax.swing.tree.TreeNode;

import prooftool.backend.direction.Direction;
import prooftool.proofrepresentation.justification.Justification;

/**
 * This abstract class must be extended by the basic elements of a proof.
 * 
 * @author dave
 * @author evm
 */
public abstract class ProofElement implements SavableElement, TreeNode {

	private Proof parent;
	
	private Justification just;
	private Direction dir;
	
	private Rectangle rect;

	/**
	 * Remove this ProofElement from the proof that it is currently a
	 * constituent of
	 */
	public abstract void remove();

	/**
	 * Add a ProofElement under this one in this one's parent
	 * 
	 * @param nextElement
	 *            The ProofElement to add to the proof that this is a part of
	 */
	public void insertAsNextLine(ProofElement nextElement) {
		assert this.parent != null;
		this.parent.addUnder(this, nextElement);
	}

	/**
	 * Get the ProofLine before this one, ignoring sibling ProofElements that
	 * are Proofs
	 * 
	 * @return The predecessor ProofLine of this ProofElement
	 */
	public ProofLine getPreviousLine() {
		if (this.parent == null) {
			return null;
		}

		int index = this.parent.getChildren().indexOf(this);

		while (index > 0) {
			index--;
			ProofElement p = this.parent.getChildren().get(index);
			
			if (p instanceof ProofLine) {
				return (ProofLine) p;
			}
		}
		
		return null;
	}

	/**
	 * Get the ProofElement succeeding this one
	 * 
	 * @return The next proofelement after this one in the proof that it lives
	 *         in, or null, if it is last
	 */
	public ProofElement getNextElement() {
		assert this.parent != null;
		return this.parent.getElementAfter(this);
	}

	/**
	 * Get the ProofElement preceding this one
	 * 
	 * @return The previous proofelement before this one in the proof that it
	 *         lives in, or null, if it is the first
	 */
	public ProofElement getPreviousElement() {
		assert this.parent != null;
		return this.parent.getElementBefore(this);
	}

	/**
	 * Return false iff this is the last element in the proof heirarchy, such
	 * that it would be shown in the lowest position on screen.
	 * 
	 * @return Whether this is the last element in the entire proof
	 */
	public boolean isLastElementInBaseProof() {
		if (this.parent == null) {
			return true;
		}
		if (this.getNextElement() == null) {
			return this.parent.isLastElementInBaseProof();
		}
		return false;
	}

	/**
	 * Get the last line in this proofelement, which is the line itself if this
	 * is a line, otherwise, the last line in the last child of this proof.
	 * 
	 * @return The last line under this element
	 */
	public ProofLine getLastLine() {
		if (this instanceof ProofLine) {
			return (ProofLine) this;
		} else {
			// This is overruled by the getLastLine in Proof
			return null;
		}
	}

	public int getIndexInParent() {
		if (this.parent == null)
			return -1;
		else
			return this.parent.getChildren().indexOf(this);
	}

	/**
	 * Get the direction for this ProofElement. This indicates the actual or
	 * permitted logical direction for nearby expressions
	 * 
	 * @return The direction of this ProofElement
	 */
	public Direction getDirection() {
		return this.dir;
	}

	public void setDirection(Direction dir) {
		this.dir = dir;
	}

	/**
	 * Get the proof that this ProofElement is a member of, or null if it is the
	 * base proof or has no parent
	 * 
	 * @return The parent proof of this ProofElement
	 */
	public Proof getParent() {
		return this.parent;
	}

	/**
	 * Get the Justification giving the rationale and formal justification
	 * permitting this ProofElement to be transformed into another
	 * 
	 * @return The justification for this line
	 */
	public Justification getJustification() {
		return this.just;
	}

	public Rectangle getRect() {
		return this.rect;
	}

	/**
	 * Set the justification for this element to be the given justification
	 * 
	 * @param justification
	 *            The justification to give this proofelement
	 */
	public void setJustification(Justification justification) {
		this.just = justification;
	}

	public void setParent(Proof newParent) {
		this.parent = newParent;
	}
	
	public void setRect(Rectangle rect) {
		this.rect = rect;
	}
}
