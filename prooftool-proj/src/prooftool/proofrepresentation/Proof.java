package prooftool.proofrepresentation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.tree.TreeNode;

import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.AbbreviatedScope;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Scope;
import prooftool.gui.prooftree.ExpressionInputBox;
import prooftool.proofrepresentation.ProofRepresentationBits.MonotonicContext;
import prooftool.proofrepresentation.ProofRepresentationBits.ProofSubtype;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.justification.ZoomInJustification;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;
import prooftool.util.Main;
import prooftool.util.ProofRep;
import prooftool.util.objects.Path;

/**
 *
 * @author dave
 * @author evm
 */

public class Proof extends ProofElement implements Cloneable {
	
	// these fields need to be regenerated
	private List<ProofElement> children;
	// this is the entire context up to this proof, excluding recently gained context
	private List<List<Expression>> fullContext;
	private List<List<Expression>> parentProofContext;
	
	private List<Identifier> scopeVars = new ArrayList<Identifier>();
	
	private ProofOptions options;

	/************************************************
	 * Constructor
	 * 
	 * @param dir
	 *            Direction of the proof
	 * @param firstLine
	 *            The first line of the proof
	 * @param j
	 *            The justification
	 * @param t
	 *            If this is a subproof, what is its type
	 */
	public Proof(Direction dir, Justification j, ProofOptions options) {
		this.setDirection(dir);
		this.setJustification(j);
		
		this.options = options;

		// build the first line
		this.children = new ArrayList<ProofElement>();
	}
	
	public Proof(String s) throws Exception{
		this.children = new ArrayList<ProofElement>();
		this.loadElement(s);
	}

	public Proof() {
		this.children = new ArrayList<ProofElement>();
	}

	/**
	 * Make a nice formatted unicode string for this proof, with tab
	 * indentations
	 * 
	 * @return a stringification of this proof
	 */
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		for (int i = 0; i < this.children.size(); i++) {
			// Get the children
			Object obj = this.children.get(i);

			// Branch depending on their type
			if (obj instanceof Proof) {
				Proof p = (Proof) obj;
				String subproof = ProofRepresentationBits.tabCharacter + p.toString();
				subproof = subproof.replace("\n", "\n" + ProofRepresentationBits.tabCharacter);
				ret.append(subproof);
			} else if (obj instanceof ProofLine) {				
				ProofLine p = (ProofLine) obj;
				//We need to check if this is the editornode, if it is we dont care about saving it
				if (p.getPanel() instanceof ExpressionInputBox) {
					continue;
				}
				
				ret.append(p.toString());
			} else {
				ret.append("???"); // indicating that the type of this thing is
									// unknown
				ret.append(obj.toString());
			}
			ret.append("\n");
		}

		String str = ret.toString();
		return str.trim();
	}

	@Override
	public void remove() {
		assert super.getParent() != null;
		assert super.getParent().children.contains(this);

		ProofElement prev = this.getPreviousElement();
		if (prev != null) {
			prev.setJustification(ProofRepresentationBits.InvalidJustification);
		}

		/*
		 * Remove all of your children, in case this has side effects but it
		 * probably won't for (int i = children.size() - 1; i >= 0; i--) {
		 * get(i).remove(); }
		 */

		super.getParent().children.remove(this);
	}

	@Override
	public Proof clone() {
		// Needs fixing
		Proof ret = new Proof(this.getDirection(), this.getJustification(), this.options.clone());
		
		for (int i = 0; i < children.size(); i++) {
			ProofElement kid = children.get(i);
			if (kid instanceof Proof) {
				Proof p = ((Proof) kid).clone();
				ret.addAtBottom(p);
			} else {
				ProofLine pl = (ProofLine) kid;
				
				//Make sure we are not cloning the editorNode
				if (pl.getPanel() instanceof ExpressionInputBox) {
					continue;
				}
				
				ProofLine p = pl.clone();
				ret.addAtBottom(p);
			}
		}

		ret.parentProofContext = this.parentProofContext;
		ret.fullContext = this.fullContext;
		return ret;
	}

	@Override
	public ProofLine getLastLine() {
		return children.get(children.size() - 1).getLastLine();
	}
	
	@Override
	public String toSaveString() {
		return this.toSaveStringHelper(0).toString();
	}
	
	// Methods needed to be implemented from TreeNode
	@Override
	public TreeNode getChildAt(int childIndex) {
		return this.children.get(childIndex);
	}

	@Override
	public int getChildCount() {
		return this.children.size();
	}

	@Override
	public int getIndex(TreeNode node) {
		return this.children.indexOf(node);
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public boolean isLeaf() {
		return this.children.isEmpty();
	}

	@Override
	public Enumeration<ProofElement> children() {
		return Collections.enumeration(this.children);
	}

	// End of Methods needed by TreeNode
	
	public ProofOptions getProofOptions() {
		return this.options;
	}

	public List<ProofElement> getChildren() {
		return this.children;
	}
	
	/**
	 * Add an element into this Proof as the line following from some element
	 * 
	 * @param element
	 *            The element underneath which we are adding a new ProofElement
	 * @param nextElement
	 *            The element to add
	 */
	public void addUnder(ProofElement element, ProofElement newElement) {
		assert this.children.contains(element);
		assert newElement.getParent() == null;

		// perform the insertion
		int index = children.indexOf(element);
		children.add(index + 1, newElement);
		newElement.setParent(this);
	}
	
	public void add(ProofElement newElement) {
		// must have at least a single line in the proof
		assert !children.isEmpty() || newElement instanceof ProofLine; 
		children.add(newElement);
		newElement.setParent(this);
	}
	
	/**
	 * Add a new ProofElement at the bottom of this proof
	 * 
	 * @param newElement
	 *            The new element to add
	 */
	private void addAtBottom(ProofElement newElement) {
		if (newElement != null) {
			children.add(newElement);
			newElement.setParent(this);
		}
	}
	
	/**
	 * If this is a subproof being opened in a new tab
	 * we want to have the ability to include the parents
	 * context.
	 * 
	 * @author context
	 * 				The parents context to be added
	 */
	public void setParentContext(List<List<Expression>> context) {
		assert context != null;
		assert this.options.getProofSubtype() == ProofSubtype.SUBPROOF;
		
		this.parentProofContext = context;
	}
	
	/**
	 * Get the ProofElement immediately before the given one
	 * 
	 * @param element
	 *            The proof element whose predecessor we want
	 * @return The element immediately before element, or null if it is the last
	 *         element in this proof
	 */
	protected ProofElement getElementBefore(ProofElement element) {
		assert this.children.contains(element);
		int index = this.children.indexOf(element);
		
		if (index == 0) {
			return null;
		}
		
		return this.children.get(index - 1);
	}
	
	/**
	 * Get the ProofElement immediately after the given one
	 * 
	 * @param element
	 *            The proof element whose successor we want
	 * @return The element immediately following element, or null if it is the
	 *         last element in this proof
	 */
	protected ProofElement getElementAfter(ProofElement element) {
		assert children.contains(element);
		int index = children.indexOf(element);

		if (index >= children.size() - 1) {
			return null;
		}

		return this.children.get(index + 1);
	}
	
	/**
	 * Determine if this is the base proof, as all proofs not embedded in some
	 * other proof are
	 * 
	 * @return Whethert this is a base proof or not
	 */
	public boolean isBaseProof() {
		return super.getParent() == null;
	}

	public boolean isEmptyProof() {
		if (this.isBaseProof()) {
			switch (this.children.size()) {
			case 0:
					return true;
			case 1:
				//Check for EditorNode
				ProofElement kid = this.children.get(0);
				if (kid instanceof ProofLine) {
					ProofLine kidPL = (ProofLine) kid;
					if (kidPL.getPanel() instanceof ExpressionInputBox) {
						return true;
					}
				}
				
				//Fall over to false
			default:
				return false;
			}
			
		}
		
		return false;
	}
	
	public boolean isProofDebt() {
		ProofLine line = this.getPreviousLine();
		
		if (line != null) {
			return !(line.getJustification() instanceof ZoomInJustification);
		}
		
		return false;
	}
	
	protected boolean isEqualsProof() {
		for (int i = 1; i < this.children.size(); i++) {
			if ((children.get(i) instanceof ProofLine)
					&& !(children.get(i).getDirection().equals(ProofRepresentationBits.equalsDir))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Proof do not need to store their direction, since the connective of the
	 * first line of each proof always contains the direction
	 */
	public Direction getDirection() {
		if (this.children.isEmpty()) {
			return ProofRepresentationBits.equalsDir;
		}

		return this.children.get(0).getDirection();
	}

	/**
	 * A simple-stupid procedure to check if a specification has been refined
	 * somewhere in the proof. We can of course check each line, and if found
	 * return a list of all the expressions that refine the specification. This
	 * is rather time consuming. We instead check if there is a (sub-)proof that
	 * starts with the given specification, and has the implied by direction. In
	 * this case we return the last line of this proof.
	 * 
	 * @param spec
	 * @return
	 */
	public Expression getRefinement(Expression spec) {
		Direction rev_imp = ExpressionUtils.revImpDir;
		Direction imp = ExpressionUtils.impDir;
		Expression refinement = null;

		// is the first line equal to spec
		if (this.children.size() > 1 && ((ProofLine) this.children.get(0)).getExpn().equals(spec)) {
			int refIndex = this.children.size() - 1;
			while (!(this.children.get(refIndex) instanceof ProofLine)) {
				refIndex = refIndex - 1;
			}
			
			refinement = ((ProofLine) this.children.get(refIndex)).getExpn();
			Proof clone;
			clone = (Proof) this.clone();
			Application app = (Application) ProofRep.proves((ProofLine) clone.children.get(refIndex));
			Direction provesDir = new ExpnDirection(app.getFunction()
					.toString());

			// var x:nat·x:=5.(letp Q=(x:=x+1)·x<3.Q)
			// if its just a zoom-in proof we don't yet have a refinement
			if (refIndex != 0
					&& (provesDir.equals(imp) || provesDir.equals(rev_imp))) {
				return refinement;
			}
		}

		// look in sub-proofs if not found
		for (ProofElement child : this.children) {
			if (child instanceof Proof) {
				refinement = ((Proof) child).getRefinement(spec);
				if (refinement != null) {
					break;
				}
			}
		}
		return refinement;
	}

	/**
	 * Get the context from all parent proofs, all in one list. The most
	 * recently acquired context is earliest in the list.
	 * 
	 * @return The context of this line
	 */
	private void generateFullContext() {
		Map<Expression, Expression> usableContextMap = new HashMap<Expression, Expression>();
		Map<Expression, Expression> greyContextMap = new HashMap<Expression, Expression>();
		List<Expression> parentUsableContext = new ArrayList<Expression>();
		List<Expression> immediateContext = new ArrayList<Expression>();
		ProofLine prevLine = this.getPreviousLine();

		// add the parent's grey and usable context, if a parent exists
		if (super.getParent() != null) {
			// adding one by one because of the stupid Law/Expression
			// incompatibility. Law really should have been an abstract class
			// extended by Expression
			List<List<Expression>> context = super.getParent().getFullContext();
			
			for (Expression l : context.get(0)) {
				parentUsableContext.add(l);
			}
			for (Expression l : context.get(1)) {
				greyContextMap.put(l, l);
			}
			
			if (context.size() == 4) {
				for (Expression l : context.get(2)) {
					parentUsableContext.add(l);
				}
				for (Expression l : context.get(3)) {
					greyContextMap.put(l, l);
				}
			}
		}

		this.addScopeFilteredContext(usableContextMap, greyContextMap, parentUsableContext, true);
		// add the context immediate to this proof, if its a zoom-in proof
		if (prevLine != null
				&& (prevLine.getJustification() instanceof ZoomInJustification)) {
			Path path = ((ZoomInJustification) prevLine.getJustification())
					.getPath();
			Expression expn = prevLine.getExpn();
			for (int i = 0; i < path.size(); i++) {
				immediateContext = expn.getContext(path.get(i));

				// make the name nicer
				for (Expression exp : immediateContext) {
					exp.setLawName("Context");
				}
				this.addScopeFilteredContext(usableContextMap, greyContextMap,
						immediateContext, false);
				expn = expn.getChild(path.get(i));
			}
		}

		// init the context lists, and add everything to them
		this.fullContext = new ArrayList<List<Expression>>();
		this.fullContext.add(new ArrayList<Expression>());
		this.fullContext.add(new ArrayList<Expression>());
		
		this.fullContext.get(0).addAll(usableContextMap.keySet());
		this.fullContext.get(1).addAll(greyContextMap.keySet());
		
		if (this.parentProofContext != null) {
			this.fullContext.add(this.parentProofContext.get(0));
			this.fullContext.add(this.parentProofContext.get(1));
		}
	}

	protected void generateScopeVars() {
		this.scopeVars = new ArrayList<Identifier>();

		if (this.isZoomInProof()) {
			ProofLine prev = this.getPreviousLine();
			Expression expn = prev.getExpn();
			Path path = ((ZoomInJustification) prev.getJustification())
					.getPath();
			int part;// = ((ZoomInJustification)prev.just).getPath().get(0);

			if (path == null) {
				return;
			}

			for (int i = 0; i < path.size(); i++) {
				part = path.get(i);

				if ((expn instanceof Scope)
						&& expn.getChild(part) == ((Scope) expn).getBody()) {
					this.scopeVars.add(((Scope) expn).getDummy().getId());
					if (expn instanceof AbbreviatedScope) {
						Identifier id = ((AbbreviatedScope) expn).getId();
						if (id == Identifiers.var || id == Identifiers.result) {
							this.scopeVars.add(((AbbreviatedScope) expn)
									.getDummyPrime().getId());
						}
					}
				}
				expn = expn.getChild(part);
			}
		}
	}

	public List<List<Expression>> getFullContext() {
		this.generateFullContext();
		return this.fullContext;
	}

	public boolean isZoomInProof() {
		ProofLine prevLine = this.getPreviousLine();
		
		if (prevLine != null) {
			return prevLine.getJustification() instanceof ZoomInJustification;
		}
		
		return false;
	}

	public void verifyAll(LawBlob lawBlob) {
		for (int i = 0; i < this.children.size(); i++) {
			ProofElement e = this.children.get(i);
			
			if (e instanceof Proof) {
				((Proof) e).verifyAll(lawBlob);
			} else if (e instanceof ProofLine) {
				ProofLine line = (ProofLine) e;
				// Check if the local environment permits the transformation of
				// line to whatever's next
				boolean reasonable = line.tryToFindJustification(lawBlob) == null;

				// If that wasn't good enough, let's try to unify by known laws
				if (!reasonable) {
					// Ask some more knowledgable class to find a unifying
					// justification for line.expr to nextline.expr
				}
			}
		}
	}

	/**
	 * Find out if any of the steps used in this subproof are unjustified. The
	 * last element is excluded if it is a proof line
	 * 
	 * @return Whether any of the steps involved in this proof are invalid
	 */
	protected boolean containsAnyInvalidSteps() {
		// TODO last element allowed to be invalid if it is a line
		for (int i = 0; i < this.children.size() - 1; i++) {
			ProofElement e = this.children.get(i);

			if (e instanceof Proof) {
				Proof ee = (Proof) e;
				if (ee.containsAnyInvalidSteps()) {
					return true;
				}
			} else {
				ProofLine ee = (ProofLine) e;
				if (ee.getJustification().isInvalid()) {
					return true;
				}
			}
		}

		ProofElement e = this.children.get(children.size() - 1);
		if (this.children.size() > 0 && e instanceof Proof) {
			return ((Proof) e).containsAnyInvalidSteps();
		}

		return false;
	}
	
	private void addScopeFilteredContext(Map<Expression, Expression> usable,
			Map<Expression, Expression> grey, List<Expression> candidate,
			boolean filter) {
		for (Expression expn : candidate) {
			usable.put(expn, expn);
			for (Identifier id : scopeVars) {

				if (filter && expn.contains(id)) {
					grey.put(expn, expn);
					usable.remove(expn);
				} else if (!filter) {
					if ((expn instanceof Application)
							&& ((Application) expn).getFunId() == Identifiers.colon
							&& expn.getChild(1).contains(id)) {
						grey.put(expn, expn);
						usable.remove(expn);
					}
				}
			}
		}
		// TODO perhaps call any context modifiers

		// adding top is useless, so remove it if its in
		usable.remove(ExpressionUtils.top);
		grey.remove(ExpressionUtils.top);
	}

	private StringBuilder toSaveStringHelper(int depth) {
		StringBuilder ret = new StringBuilder();

		ret.append(depth);
		ret.append(ExpressionUtils.savedFieldDelim);
		ret.append(this.options.getMonotonicContext());
		ret.append(ExpressionUtils.savedFieldDelim);
		ret.append(this.options.isHidden());
		ret.append(ExpressionUtils.savedFieldDelim);
		ret.append(this.options.getProofSubtype());
		
		//Save parent context if it exists
		if (this.parentProofContext != null) {
			ret.append(ExpressionUtils.savedFieldDelim);
			ret.append(this.parentProofContext.get(0).toString());
			ret.append(ExpressionUtils.savedFieldDelim);
			ret.append(this.parentProofContext.get(1).toString());
		}
		
		ret.append(ExpressionUtils.savedLineDelim);
		
		List<ProofElement> children = this.getChildren();

		for(ProofElement child : children) {
			if (child instanceof Proof) {
				ret.append(((Proof)child).toSaveStringHelper(depth+1));
			} else {
				ProofLine p = (ProofLine) child;
				
				//We need to check if this is the editornode, if it is we dont care about saving it
				if (p.getPanel() instanceof ExpressionInputBox) {
					continue;
				}
				
				ret.append(depth);
				ret.append(ExpressionUtils.savedFieldDelim);
				ret.append(p.toSaveString());
				if (!(children.get(children.size()-1).equals(child)&&depth==0)) {
					ret.append(ExpressionUtils.savedLineDelim);
				}
			}    		
		}
		
		return ret;
	}
	
	@Override
	public void loadElement(String savedProof) throws Exception{
		Scanner scan = new Scanner(savedProof);
		scan.useDelimiter(ExpressionUtils.savedLineDelim);

		String proofFields = scan.next();
		this.loadElementHelper(scan, proofFields);
	}

	private void loadElementHelper(Scanner scan, String proofFields) throws Exception {
		Scanner fieldScan = new Scanner(proofFields);    	
		fieldScan.useDelimiter(ExpressionUtils.savedFieldDelim);

		int depth = Integer.parseInt(fieldScan.next());
		String field = fieldScan.next();
		MonotonicContext mono = field.equals("null") ? null : MonotonicContext.valueOf(field);
		ProofSubtype type = null;

		field = fieldScan.next();
		boolean hidden = Boolean.parseBoolean(field);
		
		//Parse subproof type if it exists
		if (fieldScan.hasNext()) {
			field = fieldScan.next();
			type = field.equals("null") ? null : ProofSubtype.valueOf(field);
		}
		
		//Parse parent context if it exists
		if (fieldScan.hasNext()) {
			ArrayList<List<Expression>> nC = new ArrayList<List<Expression>>();
			ArrayList<Expression> c = new ArrayList<Expression>();
			ArrayList<Expression> gC = new ArrayList<Expression>();
			
			String context = fieldScan.next();
			context = context.length() != 2 ? context.substring(1, context.length() - 1) : "";
			String greyContext = fieldScan.next();
			greyContext = greyContext.length() != 2 ? greyContext.substring(1, greyContext.length() - 1) : "";
			
			Scanner t1 = new Scanner(context);
			t1.useDelimiter(",");
			Scanner t2 = new Scanner(greyContext);
			t2.useDelimiter(",");
			
			while (t1.hasNext()) {
				Expression expn = Main.parseSingle(t1.next());
				if (expn instanceof Application) {
					c.add(expn);
				}
			}
			
			while (t2.hasNext()) {
				Expression expn = Main.parseSingle(t2.next());
				if (expn instanceof Application) {
					gC.add(expn);
				}
			}
			
			if (!c.isEmpty() || !gC.isEmpty()) {
				nC.add(c);
				nC.add(gC);
				this.parentProofContext = nC;
			} else {
				this.parentProofContext = null;
			}
			
			t1.close();
			t2.close();
		}
		
		fieldScan.close();		
		
		//set the options
		this.options = new ProofOptions(mono, type, hidden);

		//regenerate some fields
		this.generateScopeVars(); //important to do this before context generation
		this.generateFullContext();
		while(scan.hasNext()) {
			String nextLine = scan.next();

			int currentDepth = Integer.parseInt(nextLine.substring(0, nextLine.indexOf(ExpressionUtils.savedFieldDelim)));
			if (currentDepth==depth || currentDepth==depth+1) {
				this.addSavedElement(depth, currentDepth, nextLine, scan);
			} else if (currentDepth+1==depth) {
				((Proof)this.getParent()).addSavedElement(currentDepth, currentDepth, nextLine, scan);
				break;
			} else {
				throw new Exception("Error loading lines: improper depth change from " + depth + " to " + currentDepth);
			}
		}
	}
	
	private void addSavedElement(int depth, int currentDepth, String nextLine, Scanner scan) throws Exception {      	
		int d = nextLine.indexOf(ExpressionUtils.savedFieldDelim);

		if (currentDepth>depth) { //proof
			Proof subProof = new Proof();
			this.add(subProof);
			subProof.loadElementHelper(scan, nextLine);
		} else if (currentDepth==depth) { //add line    	        
			this.add(new ProofLine(nextLine.substring(d+1)));
		}
	}
}
