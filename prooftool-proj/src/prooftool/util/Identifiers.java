package prooftool.util;

import java.util.HashMap;
import java.util.Map;

import prooftool.backend.laws.Identifier;

/**
 * Abstract factory for identifiers.
 * Single-keyword identifiers are to be had via method get(),
 * others can be grabbed from the public fields.
 * 
 * @author robert
 *
 */
public class Identifiers
{
	/** 
	 * this caches all identifier objects except unary minus and unary asterisk
	 */
	private static final Map<String, Identifier> identifiers = new HashMap<String, Identifier>();	
	
	public static Identifier get(int prec, boolean op, boolean infix, String name) {		
		if (!Identifiers.identifiers.containsKey(name)) {
			Identifiers.identifiers.put(name, new Identifier(prec, op, infix, name));
		}
		return Identifiers.identifiers.get(name);
	}
	
	public static Identifier getName(String name) {		
		if (!Identifiers.identifiers.containsKey(name)) {
			Identifiers.identifiers.put(name, Identifiers.get(0, false, false, name));
		}
		
		return Identifiers.identifiers.get(name);
	}
	
	public static Identifier get_infix(int prec, String name) {
		return Identifiers.get(prec, true, true, name);
	}

	public static Identifier get(String name) {
		return Identifiers.identifiers.get(name);
	}

	public final static Identifier ifthenelse 	= new Identifier(0, "if ", " then ", " else ", " end");
	public final static Identifier whiledo 		= new Identifier(0, "while ", " do ", " end");
	public final static Identifier set_brackets = new Identifier(0, "{", "}");
	public final static Identifier list_brackets= new Identifier(0, "[", "]");
	public final static Identifier scope 		= new Identifier(0, "⟨", ":", "→", "⟩");
	public final static Identifier bunch_scope 	= new Identifier(0, "⟨", ":", "↦", "⟩");
	public final static Identifier procedure_scope = new Identifier(0, "⟨", ":", "↣", "⟩");
	
	public final static Identifier triangles	= new Identifier(6, "", "⊲", "⊳", "");
	public final static Identifier commadotdot	= Identifiers.get_infix(6, ",..");
	public final static Identifier semicolondotdot	= Identifiers.get_infix(5, ";..");
	
	
	public final static Identifier fun_arrow = Identifiers.get_infix(2, "→");	
	public final static Identifier and = Identifiers.get_infix(9, "∧");
	public final static Identifier or = Identifiers.get_infix(10, "∨");
	public final static Identifier imp = Identifiers.get_infix(11, "⇒");
	public final static Identifier rev_imp = Identifiers.get_infix(11, "⇐");
	public final static Identifier gets = Identifiers.get_infix(12, ":=");	
	public final static Identifier not = new Identifier(2, "¬", "");
	
	//Level 7
	public final static Identifier colon = Identifiers.get_infix(7, ":");
	public final static Identifier rev_colon = Identifiers.get_infix(7, "::");
	public final static Identifier lt = Identifiers.get_infix(7, "<");
	public final static Identifier gt = Identifiers.get_infix(7, ">");
	public final static Identifier lte = Identifiers.get_infix(7, "≤");
	public final static Identifier gte = Identifiers.get_infix(7, "≥");
	public final static Identifier eq = Identifiers.get_infix(7, "=");
	public final static Identifier not_eq = Identifiers.get_infix(7, "≠");	
	public final static Identifier supset = Identifiers.get_infix(7, "⊃");
	public final static Identifier subset = Identifiers.get_infix(7, "⊂");
	public final static Identifier supseteq = Identifiers.get_infix(7, "⊇");
	public final static Identifier subseteq = Identifiers.get_infix(7, "⊆");
	public final static Identifier in = Identifiers.get_infix(7, "∈");
	
	public final static Identifier run = new Identifier(12, "run ", " result ", ""); 		
	public final static Identifier post_exclamation = new Identifier(12, "", "!"); 
	public final static Identifier infix_exclamation = new Identifier(12, "", "!", "");
	public final static Identifier post_question = new Identifier(12, "", "?");

	public final static Identifier power = get(2, true, true, "↑");
	public final static Identifier times = get(3, true, true, "×");
	public final static Identifier divide = get(3, true, true, "/");
	public final static Identifier plus = get(4, true, true, "+");
	public final static Identifier minus = get(4, true, true, "-");
	public final static Identifier oplus = get(4, true, true, "⊕");
	
	public final static Identifier unary_minus = new Identifier(2, "-", "");
	public final static Identifier infix_minus = new Identifier(4, "", "−", "");
	public final static Identifier unary_asterisk = new Identifier(2, "*", "");
	public final static Identifier infix_asterisk = new Identifier(2, "","*", "");
	
	public final static Identifier forall = new Identifier(13, "∀", "");
	public final static Identifier exists = new Identifier(13, "∃", "");
	public final static Identifier prod = new Identifier(13, "∏", "");
	public final static Identifier sum = new Identifier(13, "∑", "");
	public final static Identifier sol = new Identifier(13, "§", "");
	public final static Identifier min = new Identifier(13, "MIN", "");
	public final static Identifier max = new Identifier(13, "MAX", "");
	
	public final static Identifier dep_comp = Identifiers.get_infix(13, ".");
	public final static Identifier indep_comp = Identifiers.get_infix(13, "||");
	
	public final static Identifier let = new Identifier(14, "let ", "=", "·", "");
	public final static Identifier letp = new Identifier(14, "letp ", "=", "·", "");
	public final static Identifier var = new Identifier(14, "var ", ":", "·", "");
	public final static Identifier result = new Identifier(14, "result ", ":", "·", "");
	public final static Identifier ivar	= new Identifier(14, "ivar ", ":", "·", "");
	public final static Identifier chan	= new Identifier(14, "chan ", ":", "·", "");
	public final static Identifier frame = new Identifier(14, "frame ","·", "");
	

	
	public final static Identifier lambda = new Identifier(13, "λ", "");
	
	//public final static Identifier application	= new Identifier(1, "", " ","");
	// the empty keywords on ‘triangles’ is for printing with ·|·|· pattern!
	// U+01C3
	// U+0021
}


