package prooftool.util;

import java.util.StringTokenizer;

import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Scope;
import prooftool.proofrepresentation.Proof;
import prooftool.proofrepresentation.ProofElement;
import prooftool.proofrepresentation.ProofLine;
import prooftool.proofrepresentation.ProofRepresentationBits.MonotonicContext;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.justification.ZoomInJustification;
import prooftool.util.objects.Path;
import prooftool.util.objects.ZoomInfo;

public class ProofRep {
	
	public static ZoomInfo calcOffsets(Expression expn, Path childPath) {
		Expression part = expn.at(childPath.clone());
		int start = expn.offset(childPath);    			
		String line = expn.toString().substring(0,start); 
		int count = line.length() - line.replace("·", "").length();    	    	
		start = start + count;

		line = part.toString();
		count =  line.length() - line.replace("·", "").length();
		int end = start + part.toString().length();
		end = end + count;

		return new ZoomInfo(start, end, childPath);   			
	}

	public static int calcShiftStart(Expression expn, Path childPath){    	  
		int start = expn.offset(childPath);    			
		String line = expn.toString().substring(0,start); 
		int count =  line.length() - line.replace("·", "").length();
		return count;
	}

	public static int calcShiftEnd(Expression expn, Path childPath){    	  
		Expression part = expn.at(childPath.clone());		
		String line = part.toString(); 
		int count = line.length() - line.replace("·", "").length();
		return count;
	}

	public static Direction typedDirection(Direction currentDir, Expression expn,int part, boolean reverse) {
        Direction newDirection = currentDir;

        //special handling for bunch inclusion
        if (expn instanceof Application && ((Application)expn).getFunId() == Identifiers.colon) {
        	newDirection = reverse? ExpressionUtils.revColonDir:ExpressionUtils.colonDir;
        } else if (expn instanceof Application && ((Application)expn).getFunId() == Identifiers.rev_colon) {
        	newDirection = (!reverse)? ExpressionUtils.revColonDir:ExpressionUtils.colonDir;
        } else if ((expn.getType() != null) && (expn.getChild(part).getType() != null)) {
        	
            String[] oldTypeDirections = Symbol.getTypeDir(expn.getType());
            String[] newTypeDirections = Symbol.getTypeDir(expn.getChild(part).getType());
            
            if (reverse) {
            newDirection = new ExpnDirection(Symbol.reverseDirections.get(
                    currentDir.toString()));
            } else {
            	newDirection = currentDir;
            }
                      
            if (oldTypeDirections != null && newTypeDirections != null) {
            	
                for (int i = 0; i < oldTypeDirections.length; i++) {
                	
                    if ((oldTypeDirections[i] != null) && (oldTypeDirections[i].equals(newDirection.toString()))) {
                        newDirection = new ExpnDirection(newTypeDirections[i]);
                    }
                }
            }
        } else {
        	
        	if (reverse) {
        		newDirection = new ExpnDirection(Symbol.reverseDirections.get(
        				currentDir.toString()));
        	} else {
        		newDirection = currentDir;
        	}
        	//newDirection = ExpressionUtils.eqDir;
        }
        return newDirection;
    }
	
	public static Direction modifyDirection(Direction old, Expression expn, int i) {
		MonotonicContext newMono = determineMono(expn, i);

		if (newMono == MonotonicContext.NEUTRAL) {
			return ExpressionUtils.eqDir;
		} else {
			return ProofRep.typedDirection(old, expn, i, newMono == MonotonicContext.NEGATIVE);
		}
	}

	public static MonotonicContext determineMono(Expression expn, int part) {
		MonotonicContext[] monoContexts = null;
		MonotonicContext newMono;

		if (expn instanceof Application) {
			Expression op = ((Application) expn).getFunction();
			monoContexts = Symbol.monotoneMap.get(op.toString());
		} 

		if (monoContexts != null) {
			newMono = monoContexts[part];
		} else {
			if (expn.getClass().getName().equals(Scope.class.getName())) {
				newMono = MonotonicContext.POSITIVE;
			} else {
				newMono = MonotonicContext.NEUTRAL;
			}
		}
		
		return newMono;
	}
	
	public static Justification loadJustification(String s) throws Exception{
		String del = ExpressionUtils.savedFieldDelim;
		StringTokenizer st = new StringTokenizer(s, del);
		
		int index = Integer.parseInt(st.nextToken());
		if (index == 0) {
			return new Justification(s);
		} else if (index == 1) {
			return new ZoomInJustification(s);
		} else {
			throw new Exception("Unrecognized justification class index");
		}
	}
	
	public static Expression proves(ProofLine focus) {
		assert focus.getParent() != null;
		Proof parent = focus.getParent();
		ProofLine prev = focus;
		Expression newexpn = focus.getExpn().clone();
		Expression theorem = null;

		if (parent.getParent() != null) {
			prev = (ProofLine) parent.getPreviousElement();

			while (parent.getParent() != null
					&& (prev.getJustification() instanceof ZoomInJustification)) {
				Path path = ((ZoomInJustification) prev.getJustification())
						.getPath();
				newexpn = prev.getExpn().clone().replace(path, newexpn);
				parent = parent.getParent();

				if (parent.getParent() != null) {
					prev = (ProofLine) parent.getPreviousElement();
				}
			}
		}

		// if we have only 1 line/empty proof, nothing is proven
		// also, if we are in a non-zoomed in proof (such as domain check)
		// then at least for now we return no theorem
		if (!parent.isBaseProof() || parent.getChildren().size() <= 1) {
			return theorem;
		}

		// Only 3 possible connectives can be in one proof (ignoring subproofs):
		// equality, non-strict direction, strict direction (example =,≤,<)
		// If we only see equalities, connectives is equals. If we see a
		// non-strict
		// direction (it must be the same as the direction of the proof), then
		// the connective is non strict. If we see anything else, it must be the
		// strict connective, so strict becomes the connective

		int focusIndex = prev.getIndexInParent();
		Direction equalsDir = ExpressionUtils.eqDir;
		Direction connectiveSoFar = equalsDir;

		for (int i = 1; i <= focusIndex; i++) {
			if (parent.getChildren().get(i) instanceof ProofLine) {
				Direction dir = parent.getChildren().get(i).getDirection();

				if (dir.equals(parent.getDirection())) {
					connectiveSoFar = dir;
				} else if (!parent.getChildren().get(i).getDirection().equals(equalsDir)) {
					connectiveSoFar = dir;
					break;
				}
			}
		}

		assert parent.getChildren().get(0) instanceof ProofLine;
		// if we are now at the base proof, theorem

		return ExpressionUtils.parse("("
				+ ((ProofLine) parent.getChildren().get(0)).getExpn() + ")"
				+ connectiveSoFar.toString() + "(" + newexpn + ")");
	}
	
	/**
	 * returns the height of the current cell height, this method should be more
	 * general to handle width as well
	 */
	public static int getCellHeight(ProofElement pe) {
		if (pe instanceof ProofLine) {
			ProofLine pl = (ProofLine) pe;
			return pl.getPanel().getPreferredSize().height;
		} else {
			Proof p = (Proof) pe;
			
			if (p.getProofOptions().isHidden()) {
				return p.getRect().height;
			} else {
				int heightSum = 0;
				
				for (ProofElement children : p.getChildren()) {
					heightSum += ProofRep.getCellHeight(children);
				}
				
				return heightSum;
			}
			
		}
	}
}
