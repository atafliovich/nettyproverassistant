package prooftool.util.objects;

public class DirectionInfo {
	
	private String reverseDirection;	
	private String[] compatibleConnectives;
	
	public DirectionInfo (String revDir, String[] compat) {
		this.reverseDirection = revDir;
		this.compatibleConnectives = compat;
	}
	
	public String getReverseDirection() {
		return this.reverseDirection;
	}

	public String[] getCompatibleConnectives() {
		return this.compatibleConnectives;
	}
}
