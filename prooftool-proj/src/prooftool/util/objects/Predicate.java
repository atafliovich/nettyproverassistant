package prooftool.util.objects;

public interface Predicate<T> {
	public boolean apply(T t);
}
