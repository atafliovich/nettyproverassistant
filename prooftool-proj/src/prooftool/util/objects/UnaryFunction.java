package prooftool.util.objects;

public interface UnaryFunction<Argument, Result> {
	public Result apply(Argument argument);
}
