package prooftool.util.objects;

import java.util.Stack;

/**
 * A Path is just a stack of integers, used to trace from an expression down to
 * one of its subexpressions.
 */
public class Path extends Stack<Integer> {
	private static final long serialVersionUID = -2652599318423656685L;

	public Path() {
		super();
	}

	public Path(int i) {
		super();
		
		this.push(i);
	}

	public Path(int[] p) {
		super();
		
		for (int i : p) {
			push(i);
		}
	}

	@Override
	public Path clone() {
		Path clone = new Path();
		
		for (int i = 0; i < this.size(); i++) {
			clone.push(this.get(i));
		}
		
		return clone;
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		
		for (int i = 0; i < this.size(); i++) {
			ret.append(this.get(i));
			ret.append(',');
		}
		
		if (ret.length() != 0) {
			ret.deleteCharAt(ret.length() - 1);
		}
		
		return ret.toString();
	}
	
	public Path reverse() {
		Path rev = new Path();
		
		for (int i = this.size() - 1; i >= 0; i--) {
			rev.push(this.get(i));
		}
		
		return rev;
	}
}
