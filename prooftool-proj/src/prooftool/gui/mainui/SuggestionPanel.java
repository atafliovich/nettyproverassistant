package prooftool.gui.mainui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import prooftool.gui.Settings;
import prooftool.gui.UIBits;
import prooftool.proofrepresentation.suggestion.Suggestion;

/**
 * represeting a list item in the suggestions panel
 * @author evm
 * @author xiang
 */
public final class SuggestionPanel extends JPanel implements ActionListener, MouseListener {

	private static final long serialVersionUID = -6825732159367724202L;
	
	private NewTab newTab;
	private Suggestion suggestion;
	private Color lastBgColor = null;

	protected SuggestionPanel(boolean grey, Suggestion suggestion, NewTab newTab) {
		super();
		this.newTab = newTab;
		this.suggestion = suggestion;
		this.setLayout(new BorderLayout());
		
		this.setBackground(Color.WHITE);
		Color textColor = grey ? Color.GRAY : Color.black;

		//Direction Label & Warning Label
		JLabel dirLabel = new JLabel(this.suggestion.getDir() + " ");
		if (grey) {
			dirLabel.setIcon(UIBits.warnPic);
			dirLabel.setToolTipText("type unsafe suggestion");
		}
		
		dirLabel.setFont(UIBits.defaultFont);
		dirLabel.setBackground(Color.white);
		this.add(dirLabel, BorderLayout.LINE_START);
		
		//Expression String
		String expnStr = "";
		if (!this.suggestion.getStep().getInstantiables().isEmpty()) {
			for (Object part : this.suggestion.getExpn().toBasicParts(this.suggestion.getStep().getInstantiables())) {
				expnStr += part.toString();
			}
		} else {
			expnStr = this.suggestion.getExpn().toString();
		}

		//Expression Label
		JLabel expnLabel = new JLabel(expnStr);
		expnLabel.setFont(UIBits.defaultFont);
		expnLabel.setBackground(Color.white);
		expnLabel.setForeground(textColor);
		this.add(expnLabel, BorderLayout.CENTER);
		
		//Show source?
		//Rule Name
		String ruleString = Settings.get_Settings().isShowGeneratorSource() ? "From: " + this.suggestion.getSource() + " Law: " : "";
		ruleString += this.suggestion.getJust().toString();
		
		JLabel rule = new JLabel(ruleString);
		this.add(rule, BorderLayout.LINE_END);
		
		this.addMouseListener(this);
	}
	
	/**
	 * overriding this method allows the panel to take up full width for the suggestion panel while
	 * having elastic height 
	 */
	@Override
	public Dimension getMaximumSize() {
		Dimension preferredSize = this.getPreferredSize();
		Dimension maximumSize = super.getMaximumSize();
		return new Dimension(maximumSize.width, preferredSize.height);
	}
	
	private void selectSuggestion() {
		try {
			this.newTab.getProofTree().useSuggestion(this.suggestion);
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this,	"Could not parse input:\n" + this.suggestion.toString(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.selectSuggestion();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Object source = e.getSource();
		if (source instanceof JComponent) {
			JComponent l = (JComponent) e.getSource();
			l.setOpaque(true);
			this.lastBgColor = l.getBackground();
			l.setBackground(UIBits.lightOrange);
		}
	}

	// turn back to white when mouse leaves
	@Override
	public void mouseExited(MouseEvent e) {
		if (lastBgColor == null) {
			return;
		}
		Object source = e.getSource();
		if (source instanceof JComponent) {
			JComponent l = (JComponent) e.getSource();
			l.setBackground(lastBgColor);
		}
	}

	// set focused when clicked
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON1) {
			this.selectSuggestion();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}
