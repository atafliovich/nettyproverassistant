package prooftool.gui.prooftree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.text.JTextComponent;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import plugins.PluginAPI;
import prooftool.backend.direction.Direction;
import prooftool.backend.direction.ExpnDirection;
import prooftool.gui.UIBits;
import prooftool.gui.elements.NettyButton;
import prooftool.proofrepresentation.ProofLine;
import prooftool.util.ExpressionUtils;
import prooftool.util.Main;

/**
 * @author dave
 * @author evm
 * @author xiang
 * 
 * <p> This allows the user to edit a proof. <br>
 * It has three sections. </p>
 * 
 * <ul>
 * 	<li>DirectionField - The direction of the proof</li>
 * 	<li>TextField - The main body of the proof</li>
 * 	<li>Copybutton - This copies the field from the selected proofelement</li>
 * </ul>
 * <p>When the user presses enter, netty will process the changes.</p>
 * 
 * <p><strong>Note: This uses miglayout as it's layout manager, this should be replaced as it causes resize issues.</strong></p>
 */
public class ExpressionInputBox extends JComponent implements ActionListener, KeyListener, MouseListener {

	private static final long serialVersionUID = 2623075412370005494L;

	private ProofTree proofTree;

	private RSyntaxTextArea textField;
	private JComboBox<Direction> directionField;

	/**
	 * Constructs a ExpressionInputBox which belongs to proofTree. All user input will be passed onto proofTree.
	 * 
	 * @param proofTree The proofTree UI element this belongs to.
	 */
	protected ExpressionInputBox(ProofTree proofTree) {		
		this.proofTree = proofTree;
		this.setBackground(UIBits.backgroundColor);
		
		//Direction input
		this.directionField = new JComboBox<Direction>(ExpressionUtils.directions);
		this.directionField.addKeyListener(this);
		this.directionField.addMouseListener(this);
		this.directionField.setFont(UIBits.defaultFont);
		this.setFocuslKeys(this.directionField);

		//Expression input
		this.textField = new RSyntaxTextArea(1, 25);
		PluginAPI.textfield = this.textField;
		this.textField.setEditable(true);
		this.textField.setLineWrap(true);
		this.textField.setAntiAliasingEnabled(true);
		this.textField.setFont(UIBits.defaultFont);
		this.textField.setHighlightCurrentLine(false);
		this.textField.setTabSize(3);
		this.textField.setBorder(UIBits.expressionInputBoxBorder);
		this.textField.addKeyListener(this);
		this.textField.addMouseListener(this);
		this.setFocuslKeys(this.textField);

		//Copy button
		NettyButton copyButton = new NettyButton(UIBits.copy, "Copy", "Copy the selected line to here", "copy", this);
		copyButton.setBackground(Color.WHITE);

		//Set layout and add elements
		this.setLayout(new BorderLayout());
		this.add(this.directionField, BorderLayout.LINE_START);
		this.add(this.textField, BorderLayout.CENTER);
		this.add(copyButton, BorderLayout.LINE_END);
	}
	
	/**
	 * This takes a ProofLine element and populates the fields with contents from spl.
	 * 
	 * @param spl The proofelement used.
	 */
	public void setFields(ProofLine spl) {
		this.textField.setText(spl.getExpn().toString());
		this.directionField.setSelectedItem(spl.getDirection());
	}
	
	/**
	 * Resets the textfields to blank.
	 */
	public void clearFields() {
		this.textField.setText("");
		this.directionField.setSelectedIndex(0);
	}
	
	/* Sets the tab shortcuts,
	 * Credit: http://stackoverflow.com/questions/525855/moving-focus-from-jtextarea-using-tab-key
	 */
	private void setFocuslKeys(Component c) {
		Set<KeyStroke> strokes = new HashSet<KeyStroke>(Arrays.asList(KeyStroke.getKeyStroke("pressed TAB")));
		c.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, strokes);
		strokes = new HashSet<KeyStroke>(Arrays.asList(KeyStroke.getKeyStroke("shift pressed TAB")));
		c.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, strokes);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int code = e.getKeyCode();
		if (code == KeyEvent.VK_ENTER && !e.isShiftDown()) {
			e.consume();
			
			String newLineText = textField.getText();
			String dir = directionField.getSelectedItem().toString();

			if (this.proofTree.isEditing()) {
				if (this.proofTree.editFocus(null, newLineText, new ExpnDirection(dir))) {
					this.clearFields();
					this.proofTree.stopEditingFocus();
				}
			} else {
				if (this.proofTree.addNewLine(newLineText, new ExpnDirection(dir))) {
					this.clearFields();
				}
			}
		//Pass this to newTab assuming user is trying to use a shortcut
		} else if (code == KeyEvent.VK_O && e.isControlDown()) {
			UIBits.main.switchToOpenNewTab();
		}
		PluginAPI.textfield.requestFocus();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (e.getSource() == this.textField) {
			JTextComponent field = (JTextComponent) e.getSource();

			if (e.getKeyChar() == ' ') {
				String current = field.getText();
				String unicoded = Main.toUnicode(current);
				int pos = field.getCaretPosition();
				String beforeCursor = current.substring(0, pos);
				String unicodedBeforeCursor = Main.toUnicode(beforeCursor);

				if (!unicoded.equals(current)) {
					field.setText(unicoded);
					field.setCaretPosition(pos - (beforeCursor.length() - unicodedBeforeCursor.length()));
					e.consume();
				}
			}
		} 
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equalsIgnoreCase("copy")) {
			ProofLine curr = this.proofTree.getSelectedLine();
			if (curr == null) return;
			
			this.setFields(curr);
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}
	
}
