package prooftool.gui.undomanager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import prooftool.gui.prooftree.ProofTree;

/**
 * 
 * @author evm
 *
 */
public class UndoManager {

	private enum UndoManagerState {
		UNDO, REDO, STARTUP
	};
	
	private State currState;
	private State firstNode;
	
	private ProofTree proofTree;
	private UndoManagerState state;
	
	public class UndoManagerException extends Exception {
		private static final long serialVersionUID = 5955080331368489685L;
		
		private UndoManagerException(String error) {
			super(error);
		}
	}
	
	public UndoManager(ProofTree proofTree) {
		this.currState = new State();
		this.firstNode = this.currState;
		
		this.proofTree = proofTree;
		this.state = UndoManagerState.STARTUP;
	}
	
	public boolean canUndo() {
		return currState.getPrevState() != null;
	}
	
	public boolean canRedo() {
		return currState.getNextState() != null;
	}
	
	public void doUndo() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, UndoManagerException {
		if (!this.canUndo()) {
			throw new UndoManagerException("Nothing to undo!");
		}
		
		//Set the state
		this.state = UndoManagerState.UNDO;
		
		//Do the action that will get us to the prev state
		this.currState.invokePrev(this.proofTree);
		
		//Set the currState to the prev now
		this.currState = this.currState.getPrevState();
		
		//Proof needs to be refreshed
		this.proofTree.refreshTree(true);
	}
	
	public void doRedo() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, UndoManagerException {
		if (!this.canRedo()) {
			throw new UndoManagerException("Nothing to redo!");
		}
		
		//Set the state
		this.state = UndoManagerState.REDO;
		
		// Do the action that will get us to the next state
		this.currState.invokeNext(this.proofTree);

		// Set the currState to the next now
		this.currState = this.currState.getNextState();
		
		//Proof needs to be refreshed
		this.proofTree.refreshTree(true);
	}
	
	//Used for zoomin/zoomout, they create new objects so the undomanager needs to purge
	//the old objects and replace them with the newObj.
	public void updateObject(Object newObj) {
		assert this.state == UndoManagerState.REDO || this.state == UndoManagerState.UNDO;
		
		//Find the orignal object
		Object orgObj = null;
		Object[] params = null;
		if (this.state == UndoManagerState.REDO) {
			params = this.currState.getNextState().getPrevAction().getParams();
		} else if (this.state == UndoManagerState.UNDO) {
			params = this.currState.getPrevState().getNextAction().getParams();
		}
		
		for (int i = 0; i != params.length; i++) {
			//Make sure it is the same class
			if (params[i].getClass().getName().equalsIgnoreCase(newObj.getClass().getName())) {
				//And it has the same string
				if (params[i].toString().equalsIgnoreCase(newObj.toString())) {
					//We found it!
					orgObj = params[i];
					break;	
				}
			}
		}
		
		//Now we go through the link-list to update all references!
		State s = this.firstNode;
		while (s != null) {
			Action prev = s.getPrevAction();
			Action next = s.getNextAction();
			
			if (prev != null) {
				prev.updateParams(orgObj, newObj);
			}
			
			if (next != null) {
				next.updateParams(orgObj, newObj);
			}
			
			s = s.getNextState();			
		}
	}
	
	public void actionPerformed(Method actionDid, Object[] paramsUsed, Method methodUsedToGoBack, Object[] paramsNeededToGoBack) {		
		//Create the new actions
		Action redoAction = new Action(actionDid, paramsUsed);
		Action undoAction = new Action(methodUsedToGoBack, paramsNeededToGoBack);
		
		//Make the new state linking back to the currState (before any action has happened)
		State newState = new State(this.currState, undoAction);
		this.currState.setNextState(newState, redoAction);
		
		//Make the new state the current state
		this.currState = newState;
	}
}
