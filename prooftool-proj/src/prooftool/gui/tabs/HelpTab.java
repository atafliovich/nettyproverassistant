package prooftool.gui.tabs;

import java.awt.BorderLayout;
import java.util.Map;
import java.util.regex.PatternSyntaxException;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import prooftool.gui.UIBits;
import prooftool.util.Symbol;

/**
 * <p>This tab displays usage information.</p>
 *
 * @author maghari1
 * 
 */
public class HelpTab extends JScrollPane implements DocumentListener {
	private static final long serialVersionUID = 3457603762305456450L;
	
	private class HelpTableModel extends AbstractTableModel {

		private static final long serialVersionUID = -5990366965645761484L;
		private Object[][] symbolData;
		
		private HelpTableModel() {
			this.symbolData = new Object[Symbol.symbolMap.size()][2];
			
			int i = 0;
			for (Map.Entry<String, String> mapping: Symbol.symbolMap.entrySet()) {
				this.symbolData[i][0] = mapping.getKey();
				this.symbolData[i][1] = mapping.getValue();
				
				i++;
			}
		}		
		
		@Override
		public String getColumnName(int col) {
			switch (col) {
			case 0:
				return "What to Type";
			case 1:
				return "Output";
			default:
				return "Error";
			}
		}

		@Override
		public int getColumnCount() {
			return 2;
		}

		@Override
		public int getRowCount() {
			return this.symbolData.length;
		}

		@Override
		public Object getValueAt(int row, int col) {
			return this.symbolData[row][col];
		}
		
		@Override
		public boolean isCellEditable(int row, int col) {
			return false;
		}		
	}
	
	private JTable symbolTable;
	private TableRowSorter<TableModel> rowSorter;
	private JTextField searchField;
	
	/**
	 * Creates a new HelpTab. 
	 */
	public HelpTab() {
		super();
		
		JPanel surface = new JPanel(new BorderLayout());
		this.searchField = new JTextField();
		this.searchField.getDocument().addDocumentListener(this);
				
		this.symbolTable = new JTable(new HelpTableModel());
		this.symbolTable.setFont(UIBits.lawLibraryFont);
		this.rowSorter = new TableRowSorter<>(this.symbolTable.getModel());
		this.symbolTable.setRowSorter(this.rowSorter);
		
		surface.add(searchField, BorderLayout.PAGE_START);
		surface.add(this.symbolTable, BorderLayout.CENTER);
		
		this.setViewportView(surface);			
	}
	
	private void updateFilter() {
		String text = this.searchField.getText();
		text = text.replaceAll("['\"\\\\]", "\\\\$0");
		
		try {
			if (text.trim().length() == 0) {
				this.rowSorter.setRowFilter(null);
			} else {
				this.rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
			}	
		} catch (PatternSyntaxException ex) {	
		}	   
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		this.updateFilter();
	}
	
	@Override
	public void removeUpdate(DocumentEvent e) {
		this.updateFilter();
	}
	
	@Override
	public void changedUpdate(DocumentEvent e) {
	    throw new UnsupportedOperationException("Not supported yet.");
	}
}
