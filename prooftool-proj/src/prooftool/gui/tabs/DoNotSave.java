package prooftool.gui.tabs;

import prooftool.gui.UIBits;

/**
 * <p>This class acts as the do not save option for the prompt where it asks
 * the user which instance of a proof they want to save</p>
 */
public class DoNotSave extends TabPanelControl {
	
	private static final long serialVersionUID = -1349482973846842055L;

	public DoNotSave() {
		super(UIBits.mainUIPromptDoNotSave);
	}

	@Override
	public String toString() {
		return UIBits.mainUIPromptDoNotSave;
	}
}
