package prooftool.gui.tabs;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import prooftool.gui.UIBits;
import prooftool.proofrepresentation.Proof;

/**
 * This frame window simply displays the current proof in a new window
 * 
 * @author unknown
 * @author maghari1
 */
public class PlainTextTab extends JScrollPane {

	private static final long serialVersionUID = 5191371806404272051L;
	private JTextArea area;

	public PlainTextTab() {
		this.area = new JTextArea();
		this.area.setEditable(false);
		this.area.setFont(UIBits.lawLibraryFont);
		
		this.setViewportView(this.area);
	}
	
	public void setText(Proof proof) {
		this.area.setText(proof.toString());
	}
}
