package prooftool.gui.tabs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import prooftool.gui.Settings;
import prooftool.gui.UIBits;

/**
 * <p>This sets up the settings tab options. The options themselves are saved
 * in UIBits</p>
 */
public class SettingsTab extends JPanel implements ActionListener {
	private static final long serialVersionUID = 3457603762305456450L;

	/**
	 * Creates a new SettingsTab, the JCheckBox are generated using array of Strings found in UIBits. 
	 */
	public SettingsTab() {
		super();
		this.setLayout(new MigLayout());
		
		//Make sure the lengths of the two arrays are the same
		assert UIBits.settingsActionCommand.length == UIBits.settingsDisplayStrings.length;
		boolean[] checkBoxConfig = this.generateInitCheckBoxConfig();
		
		//Create the JCheckBoxes
		for (int i = 0; i != UIBits.settingsActionCommand.length; i++) {
			JCheckBox box = new JCheckBox(UIBits.settingsDisplayStrings[i]);
			box.setActionCommand(UIBits.settingsActionCommand[i]);
			box.addActionListener(this);
			box.setSelected(checkBoxConfig[i]);
			
			this.add(box, "wrap");
		}
	}
	
	private boolean[] generateInitCheckBoxConfig() {
		Settings s = Settings.get_Settings();
		
		boolean[] config = { s.isGreySug(), s.isZoomType(), s.isHideContextPanel(), s.isHideSuggestionPanel(), s.isShowGeneratorSource() };
		return config;
	}

	/**
	 * Processes the user input, in this case a change in the settings. Sets the apporiate bits in UIBits.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		boolean option = ((JCheckBox) e.getSource()).isSelected();

		if (action.equalsIgnoreCase(UIBits.settingsActionCommand[0])) {
			Settings.get_Settings().setGreySug(option);
		} else if (action.equalsIgnoreCase(UIBits.settingsActionCommand[1])) {
			Settings.get_Settings().setZoomType(option);
		} else if (action.equalsIgnoreCase(UIBits.settingsActionCommand[2])) {
			Settings.get_Settings().setHideContextPanel(option);
		} else if (action.equalsIgnoreCase(UIBits.settingsActionCommand[3])) {
			Settings.get_Settings().setHideSuggestionPanel(option);
		} else if (action.equalsIgnoreCase(UIBits.settingsActionCommand[4])) {
			Settings.get_Settings().setShowGeneratorSource(option);
		}
	}
}
