package prooftool.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

import prooftool.backend.direction.Direction;
import prooftool.backend.laws.Expression;
import prooftool.gui.mainui.MainUI;
import prooftool.proofrepresentation.Proof;
import prooftool.proofrepresentation.ProofLine;
import prooftool.util.objects.Path;

/**
 *
 * @author dave
 * @author evm
 */
public class UIBits {
	
	// Netty Main UI
	public static MainUI main;

	
	// Application Title
	public static final String title = "Netty";
	
	// Strings
	public static final String mainUIPromptSaveNonDupTitle = "Unsaved changes detected";
	public static final String mainUIPromptSaveNonDupContent = "The following tab contains unsaved changes %s. Would you like to save these changes before exiting?";
	public static final String mainUIPromptSaveDupTitle = "Multiple instances with unsaved changes detected";
	public static final String mainUIPromptSaveDupContent = "The following tabs %s contains unsaved changes. Please selected which instance of %s you would like saved";
	public static final String mainUIPromptDoNotSave = "Do not save any instance";
	public static final String mainUISavePrompt = "There are unsaved changes in %s, would you like to save the changes?";
	
	public static final String mainUIProofNotSelectedTitle = "Unable to export subproof.";
	public static final String mainUIProofNotSelectedContent = "No subproof has been selected. Unable to export.";
	public static final String mainUIProofNoNewTabNameProvidedContent = "Name for subproof was not provided, unable to continue.";
	public static final String mainUIProofExportNewTabNameContent = "Please provide the name for the subproof.";
	
	public static final String mainUIProofImportEmptyContentTitle = "Unable to import subproof.";
	public static final String mainUIProofImportEmptyContent = "Currently opened tab is an empty proof, unable to continue.";
	public static final String mainUIProofImportSelectTabTitle = "Select tab.";
	public static final String mainUIProofImportSelectTabContent = "Please select which tab you would like to import to.";
	public static final String mainUIProofImportNoTabsContent = "No other tabs are opened to import subproof to.";
	public static final String mainUIProofImportZoomInContent = "Currently selected section has a ZoomIn, to replace please delete the subproof first.";
	public static final String mainUIProofImportProofMismatch = "Proof mismatch";
	public static final String mainUIProofImportNoLineSelected = "No line in the selected tab has been selected!";
	
	public static final String tabUINothingSelectedContent = "No subproof was selected, unable to continue!";
	
	public static final String nettySuggestionsPanelErrorZoomIn = "(Cannot generate suggestions for this line, since it is followed by a zoom in proof. You might want to delete the zoom in proof to make this line changeable.) ";
	public static final String nettySuggestionsPanelNoMatch = "(No laws matched)";
	public static final String nettySuggestionsPanelNumberMatched = "(%s laws matched)";
	
	public static final String lawLibraryAdd = "Add";
	public static final String lawLibraryRemove = "Remove";
	public static final String lawLibrarySaveLibrary = "Save Default Library";
	public static final String lawLibraryReloadLibrary = "Reload Default Library";
	public static final String lawLibraryLabel = "Law Files";
	
	public static final String pluginAdd = "Add";
	public static final String pluginRemove = "Remove";
	public static final String pluginSave = "Save";
	public static final String pluginApply = "Apply";
	
	/**
	 * <p>The JFileChooser only really has two commands, "cancel" and
	 * "approve", I renamed the buttons to reset, and openproof. This is
	 * basically to avoid confusion for potential latter programmers. The
	 * renaming happens in MainUI.java, with the UIManager.put function
	 * calls.</p>
	 */
	public static final String[] openTabActionCommand = { "CancelSelection", "ApproveSelection" };
	public static final String openTabApproveButtonText = "Open Proof";
	public static final String openTabApproveButtonToolTip = "Loads the selected Proof File";
	
	public static final String[] settingsActionCommand = { "greySug", "zoomType", "hideContext", "hideSuggestion", "showGeneratorSource" };
	public static final String[] settingsDisplayStrings = { "Hide type-unsafe suggestions", 
																			  "Enable Deep Zoom",
																			  "Hide context panel",
																			  "Hide suggestion panel",
																			  "Show which generator is generating which suggestion"
	};
	
	// Constants
	private static final int proofFontSize = 16;
	
	public static final int proofTreeIndentAmount = 16;
	public static final int proofTreeExpandBoxHeight = 30;
	
	public static final int cornerPaintSize = 5;

	// Border Layouts
	public static final Border leftLine = BorderFactory.createMatteBorder(0, 1, 0, 0, Color.black);
	public static final Border blackBox = BorderFactory.createMatteBorder(1, 0, 1, 1, Color.black);
	public static final Border redBox = BorderFactory.createLineBorder(Color.red);
	public static final Border whiteBorder = BorderFactory.createLineBorder(Color.white);
	public static final Border noBorder = BorderFactory.createEmptyBorder();
	public static final Border noBorderTabs = BorderFactory.createEmptyBorder(2, 0, 0, 0);
	public static final Border expressionInputBoxBorder = BorderFactory.createEtchedBorder(EtchedBorder.RAISED);

	// Colours
	public static final Color lightOrange = new Color(250, 209, 0);	
	public static final Color selectionColor = new Color(255, 255, 204);
	public static final Color backgroundColor = new Color(255, 255, 255);

	// Fonts
	public static Font defaultFont; 
	public static Font lawLibraryFont;
	
	//Icons
	public static Icon warnPic;
	public static Icon close;
	public static Icon copy;
	
	//CodeGen Toolbar
	public static Icon[] CodePanelToolBarImageFiles;
	
	//NewTab Toolbar
	public static  Icon[] NewTabToolBarImageFiles;
	
	//This part was changed to allow loading resources from the jar file.
	static { 
		try {
			Font dejavusans = Font.createFont(Font.TRUETYPE_FONT, UIBits.class.getResourceAsStream("/resources/DejaVuSans.ttf"));
			GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(dejavusans);
			
			UIBits.lawLibraryFont = dejavusans.deriveFont(Font.PLAIN, 12);
			UIBits.defaultFont = dejavusans.deriveFont(Font.PLAIN, proofFontSize);
			
			//icons
			warnPic = new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/warning-icon-small.png")));
			close = new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Close-icon.png")));
			copy = new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Copy-last-icon-small-0.png")));
			
			//CodeGen Toolbar
			CodePanelToolBarImageFiles = new Icon[] {
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Save-icon-small.png"))), 
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/run-icon-small.png"))) };
			
			//NewTab Toolbar
			UIBits.NewTabToolBarImageFiles = new Icon[]{
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Save-icon-small.png"))), 
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Undo-icon-small.png"))),
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Redo-icon-small.png"))), 
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Edit-icon.png"))),
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Zoom-Out-icon-small.png"))),
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/checkmark-icon-small.png"))), 
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Law-16.png"))),
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/export.png"))), 
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/import.png"))),
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/Letters-16.png"))),
			new ImageIcon(ImageIO.read(UIBits.class.getResourceAsStream("/resources/images/QED-icon-small.png")))};
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		} 
	}
	
	// Scrollbars are set to have this horizontal and vertical increment
	public static final int scrollIncrement = UIBits.proofFontSize + 10;

	// IO & Paths
	public static final String rootPath = System.getProperty("user.dir");
	public static final String separator = System.getProperty("file.separator");
	public static File currentDir = new File(System.getProperty("user.home"));

	// Codegen
	// might need to be changed
	public static final String codeFileName = "a.cpp";
	public static final String codeOutFileName = "a.out";
	public static final String codePath = UIBits.class.getResource("/resources/codegen/").getPath();
	public static final String codeCompile = codePath != null ? "g++ --std=c++0x -I "
			+ codePath
			+ " "
			+ codePath
			+ UIBits.separator
			+ UIBits.codeFileName
			+ " -o "
			+ codePath
			+ UIBits.separator
			+ UIBits.codeOutFileName
			: null;

	// Dimensions
	public static final Dimension proofElementDimension = new Dimension(960, 60);
	public static final Dimension mainWindowDimension = new Dimension(970, 650);
	
	//JLabels
	public static final JLabel contextPanelLabel = new JLabel("<html><u>Context</u></html>");
	public static final JLabel nettySuggestionsPanelDefaultLabel = new JLabel("Click on the line of the proof to see suggested next steps.");
	
	//NewTab dialog asking the user what to do after editing a line with a zoomin
	public static final String NewTabEditDialogPromptTitle = "Subproof needs editing";
	public static final String NewTabEditDialogPromptContent = "What do you want to do with the subproof?";
	public static final String[] NewTabEditDialog = {
		"Open subproof in a new tab, then remove the subproof from this tab.", "Delete the subproof.", "Discard the edit."
	};
	
	public static final int NewTabEditDialogOpenNewTab = 0;
	public static final int NewTabEditDialogDeleteSubProof = 1;
	public static final int NewTabEditDialogDiscard = 2;
	
	public static int NewTabEditDialogStringToVal(String s) {
		for (int i = 0; i != NewTabEditDialog.length; i++) {
			if (NewTabEditDialog[i].equals(s)) return i;
		}
		
		return -1;
	}
	

	public static final String[] CodePanelToolBarTextIcons = { "Save", "Run" };
	public static final String[] CodePanelToolBarToolTips = { "Save Code",
			"Run Program" };
	
	public static final String[] NewTabToolBarTextIcons = { "Save",
			"Undo", "Redo", "Edit", "Zoom-Out", "Check", "Add to Laws", "Export", "Import", "Types", "Done"};
	public static final String[] NewTabToolBarToolTips = {
			"Save the current Proof", "Undo last action",
			"Redo last undone action", "Toggles the editing state of the current line",
			"Zoom out of current context", "Recheck current proof", 
			"Save current theorem", "Export currently selected subproof to a new tab", 
			"Import the current proof in opened tab as a subproof.",
			"Choose Type", "Finish Proof" };
	public static final int NewTabToolBarSave = 0;
	public static final int NewTabToolBarUndo = 1;
	public static final int NewTabToolBarRedo = 2;
	public static final int NewTabToolBarEdit = 3;
	public static final int NewTabToolBarZoomOut = 4;
	public static final int NewTabToolBarRecheckProof = 5;
	public static final int NewTabToolBarSaveCurrent = 6;
	public static final int NewTabToolBarExportNewTab = 7;
	public static final int NewTabToolBarImportTab = 8;
	public static final int NewTabToolBarTypes = 9;
	public static final int NewTabToolBarDone = 10;
	
	//Methods from NewTab for the UndoManager
	public static final Class<?>[] proofTreeAddNewLineProofParam = { ProofLine.class, Proof.class, Boolean.class };
	public static final String proofTreeAddNewLineProofName = "addNewLine";
	public static Method proofTreeAddNewLineProofMethod;
	
	public static final Class<?>[] proofTreeAddNewLineProofLineParam = { ProofLine.class, ProofLine.class, Boolean.class };
	public static final String proofTreeAddNewLineProofLineName = "addNewLine";
	public static Method proofTreeAddNewLineProofLineMethod;
	
	public static final Class<?>[] proofTreeDeleteLineParam = { ProofLine.class, Boolean.class };
	public static final String proofTreeDeleteLineName = "deleteLine";
	public static Method proofTreeDeleteLineMethod;
	
	public static final Class<?>[] proofTreeDeleteSubProofParam = { Proof.class, Boolean.class };
	public static final String proofTreeDeleteSubProofName = "deleteSubProof";
	public static Method proofTreeDeleteSubProofMethod;
	
	public static final Class<?>[] proofTreeSetBaseProofParam = { Proof.class, ProofLine.class, Boolean.class };
	public static final String proofTreeSetBaseProofName = "setBaseProof";
	public static Method proofTreeSetBaseProofMethod;
	
	public static final Class<?>[] proofTreeZoomInParam = { ProofLine.class, Path.class, Boolean.class };
	public static final String proofTreeZoomInName = "zoomIn";
	public static Method proofTreeZoomInMethod;
	
	public static final Class<?>[] proofTreeZoomOutParam = { ProofLine.class, Boolean.class };
	public static final String proofTreeZoomOutName = "zoomOut";
	public static Method proofTreeZoomOutMethod;
	
	public static final Class<?>[] proofTreeUndoZoomOutMultiLineParam = { ProofLine.class, ProofLine.class };
	public static final String proofTreeUndoZoomOutMultiLineName = "undoZoomOutMultiLine";
	public static Method proofTreeUndoZoomOutMultiLineMethod;
	
	public static final Class<?>[] proofTreeEditLineParam = { ProofLine.class, Expression.class, Direction.class, Boolean.class };
	public static final String proofTreeEditLineName = "editLine";
	public static Method proofTreeEditLineMethod;
	
}
