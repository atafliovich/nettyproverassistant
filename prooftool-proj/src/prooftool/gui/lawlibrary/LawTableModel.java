package prooftool.gui.lawlibrary;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import prooftool.backend.laws.Expression;

/**
 * 
 * @author maghari1
 *
 */
public class LawTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 6560440117434717882L;

	private List<Expression> laws;
	
	public LawTableModel(List<Expression> laws) {
		super();
		this.laws = laws;
	}
	
	@Override
	public int getRowCount() {
		if (this.laws == null) return 0;
		
		return this.laws.size();
	}

	@Override
	public int getColumnCount() {
		return 2;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return "Law Name";
		case 1:
			return "Parsed Law";
		}
		
		return "NULL";
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Expression law = this.laws.get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			return law.getLawName();
		case 1:
			return law.toString();
		}
		
		return "NULL";
	}

}
