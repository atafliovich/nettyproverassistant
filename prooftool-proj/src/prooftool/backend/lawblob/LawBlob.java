package prooftool.backend.lawblob;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.laws.AbbreviatedScope;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;

/**
 * 
 * @auther unknown
 * @author evm
 *
 */
public class LawBlob {		
	//----------------------------- Blob root field ----------------------------------\\ 
	
	//Maps an index to the expression to instantiate, direction, and free variables
	private Map<Integer,LawInfo> lawMap;
	
	//mapping of INSTANTIABLE LAW variables to the expressions they unified with
	private Map<VarIndex,Expression> subst;
	
	//mapping of LOCAL variables to the expressions they unified with
	private Map<VarIndex,Expression> scopeSubst;
	
	//This keeps track of variables we FUNCTIONIZED. All these variables are also in subst
	private Map<VarIndex,Scope> functionized;
	
	//This is just for efficiently cleaning the maps at the end of unification
	private Map<VarIndex,Expression> current;	
	//a duplicate of the keys used in subst, used to clean subst at the end
	
	private List<Expression> inputLaws;
	//----------------------------- ------------------- ----------------------------------\\
	
	private List<LawBlob> children;
	
	private Map<Literal,List<Integer>> litMap;
	private Map<Variable,List<Integer>> varMap;
	private Map<Variable,List<Integer>> instVarMap;
	
	//these two are for the unified algebra solution
	private Map<Variable,List<Integer>> locFunMap;
	private Map<Variable,Variable> locVarMap;
	
	private Map<Identifier,List<Integer>> scopeMap;
	//applications are treated simply with subnodes

	public LawBlob () {
		this.lawMap = new HashMap<Integer,LawInfo>();
		this.subst = new HashMap<VarIndex,Expression>();
		this.scopeSubst = new HashMap<VarIndex,Expression>();	
		this.functionized = new HashMap<VarIndex,Scope>();
		this.current = new HashMap<VarIndex,Expression>();	
		
		this.inputLaws = new ArrayList<Expression>();
		this.children = new ArrayList<LawBlob>();
		this.litMap = new HashMap<Literal,List<Integer>>();
		this.varMap = new HashMap<Variable,List<Integer>>();
		this.instVarMap = new IdentityHashMap<Variable,List<Integer>>();
		this.locFunMap = new IdentityHashMap<Variable,List<Integer>>();
		this.locVarMap = new IdentityHashMap<Variable,Variable>();
		this.scopeMap = new HashMap<Identifier,List<Integer>>();
	}
	
	private LawBlob (LawBlob parent) {
		this.lawMap = parent.lawMap;
		this.subst = parent.subst;
		this.scopeSubst = parent.scopeSubst;		
		this.functionized = parent.functionized;
		this.current = parent.current;		
		
		this.inputLaws = new ArrayList<Expression>();
		this.children = new ArrayList<LawBlob>();
		this.litMap = new HashMap<Literal,List<Integer>>();
		this.varMap = new HashMap<Variable,List<Integer>>();
		this.locFunMap = new IdentityHashMap<Variable,List<Integer>>();
		this.instVarMap = new IdentityHashMap<Variable,List<Integer>>();
		this.locVarMap = new IdentityHashMap<Variable,Variable>();
		this.scopeMap = new HashMap<Identifier,List<Integer>>();
	}
	
	public List<Expression> getInputLaws() {
		return this.inputLaws;
	}
	
	/**
	 * Add a law to this law blob. This also adds up to 6 variants, if possible.
	 * It is expected that input laws also have their types made. 
	 * 
	 */	
	public void add(Expression law) {
		//make six clones for different variants
		this.inputLaws.add(law);

		//TODO remove universal quantifiers one by one until we ge to the body, creating
		//several laws. That way, we don't need to have exaclty the body for a law to match
		//instead we can have some universal quantifiers match too
		Expression lawClone;
		int n;
		LawInfo info;
		Direction lawDir = law.getDirection();
		Expression top = ExpressionUtils.top.clone();
		Expression body = law.getLawBody();
		
		//add the law body, referencing ⊤   (A≥B = ⊤)
		n = lawMap.size();		
		info = new LawInfo(top, law);		
		info.getLaw().setLawName(law.getLawName());
		lawMap.put(n, info);
		
		for (Variable v : law.getLawVars()) {
			subst.put(new VarIndex(n,v), null);
		}
		
		this.add(body,n);
		
		//add ⊤, referencing the law body	(⊤ = A≥B)	
		lawClone = law.clone();
		law.copyTypes(lawClone);
		law = lawClone;
		body = law.getLawBody();
		n = lawMap.size();		
		info = new LawInfo(body, law, law.getLawVars(), ExpressionUtils.eqDir);
		info.getLaw().setLawName(law.getLawName());
		lawMap.put(n, info);
		this.add(top, n);
		
		//if the law is a negation (¬A) add (A = ⊥)
		if (body instanceof Application && ((Application)body).getFunId() == Identifiers.not){
			lawClone = law.clone();
			law.copyTypes(lawClone);
			law = lawClone;
			body = law.getLawBody().getChild(0);			
			
			n = lawMap.size();
			info = new LawInfo(ExpressionUtils.bottom, law);
			info.getLaw().setLawName(law.getLawName());
			lawMap.put(n, info);
			for (Variable v : law.getLawVars()) {
				subst.put(new VarIndex(n,v), null);
			}
			this.add(body,n);
		}
		
		//if this law has two sides, add more variants
		if (lawDir!=null) {
			assert body instanceof Application;
			
			try {
				Expression reverseLaw = ExpressionUtils.reverseLaw(law);
				reverseLaw.makeTypes(new ArrayList<Expression>());
				body = reverseLaw.getLawBody();
				Expression leftSide = ((Application) body).getChild(0);
				Expression rightSide = ((Application) body).getChild(1);
				List<Variable> rightUnique = ExpressionUtils.getUniqueVars(rightSide, leftSide, reverseLaw.getLawVars());
				List<Variable> leftUnique = ExpressionUtils.getUniqueVars(leftSide, rightSide, reverseLaw.getLawVars());
				
				//add right side, reference left   (A≥B)
				n = lawMap.size();		
				info = new LawInfo(leftSide, law, leftUnique, lawDir);
				info.getLaw().setLawName(law.getLawName());
				lawMap.put(n, info);
				
				for (Variable v : reverseLaw.getLawVars()) {
					subst.put(new VarIndex(n,v), null);
				}
				
				this.add(rightSide, n);
				
				//add left side, reference right    (B≤A)
				
				lawClone = reverseLaw.clone();
				reverseLaw.copyTypes(lawClone);
				reverseLaw = lawClone;
				body = reverseLaw.getLawBody();
				leftSide = ((Application) body).getChild(0);
				rightSide = ((Application) body).getChild(1);
				rightUnique = ExpressionUtils.getUniqueVars(rightSide, leftSide, reverseLaw.getLawVars());
				leftUnique = ExpressionUtils.getUniqueVars(leftSide, rightSide, reverseLaw.getLawVars());
				
				n = lawMap.size();		
				info = new LawInfo(rightSide, law, rightUnique, reverseLaw.getDirection());
				info.getLaw().setLawName(law.getLawName());
				lawMap.put(n, info);
				
				for (Variable v : reverseLaw.getLawVars()) {
					subst.put(new VarIndex(n,v), null);
				}
				
				this.add(leftSide, n);
				
				//add the law body, referencing ⊤ (B≤A = ⊤)
				lawClone = reverseLaw.clone();
				reverseLaw.copyTypes(lawClone);
				reverseLaw = lawClone;
				body = reverseLaw.getLawBody();
				leftSide = ((Application) body).getChild(0);
				rightSide = ((Application) body).getChild(1);
				rightUnique = ExpressionUtils.getUniqueVars(rightSide, leftSide, reverseLaw.getLawVars());
				leftUnique = ExpressionUtils.getUniqueVars(leftSide, rightSide, reverseLaw.getLawVars());
				
				n = lawMap.size();			
				info = new LawInfo(top, law);
				info.getLaw().setLawName(law.getLawName());
				lawMap.put(n, info);
				for (Variable v : reverseLaw.getLawVars()) {
					subst.put(new VarIndex(n,v), null);
				}
				
				this.add(body,n);
				
				//add ⊤, referencing the law body	(⊤ = B≤A)	
				lawClone = reverseLaw.clone();
				reverseLaw.copyTypes(lawClone);
				reverseLaw = lawClone;
				body = reverseLaw.getLawBody();
				leftSide = ((Application) body).getChild(0);
				rightSide = ((Application) body).getChild(1);
				rightUnique = ExpressionUtils.getUniqueVars(rightSide, leftSide, reverseLaw.getLawVars());
				leftUnique = ExpressionUtils.getUniqueVars(leftSide, rightSide, reverseLaw.getLawVars());
				
				n = lawMap.size();		
				info = new LawInfo(body, law, reverseLaw.getLawVars(), ExpressionUtils.eqDir);
				info.getLaw().setLawName(law.getLawName());
				lawMap.put(n, info);
				this.add(top, n);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//TODO check for duplicates. not strictly necessary though
	}
	
	
	/**
	 * Add all the laws to this law blob. This also adds up to 6 variants, if possible.
	 * It is expected that input laws also have their types made. 
	 * 
	 * Also calls makeTypes on all the laws.
	 */	
	public void addAllLaws(List<Expression> toAdd) {
		for (Expression exp: toAdd) {
			this.add(exp);
		}
		
		for (Expression exp: toAdd) {
			exp.makeTypes(toAdd);
		}
	}
	
	/**
	 * In this add method we add less variants. Only a single direction, 
	 * and not variants that increase the length of the expression.
	 * 
	 * @param law
	 */
	public void addHeuristic(Expression law) {
		this.inputLaws.add(law);	
		Expression lawClone;
		int n;
		LawInfo info;
		Direction lawDir = law.getDirection();
		Expression top = ExpressionUtils.top;
		Expression body = law.getLawBody();

		//add the law body, referencing ⊤   (A≥B = ⊤)
		n = lawMap.size();			
		info = new LawInfo(top, law);
		lawMap.put(n, info);
		
		for (Variable v : law.getLawVars()) {
			subst.put(new VarIndex(n,v), null);
		}
		
		this.add(body,n);


		//if this law has two sides, add the variant that goes from left to right
		if (lawDir!=null) {
			assert body instanceof Application;
			try {
				Expression reverseLaw = ExpressionUtils.reverseLaw(law);
				reverseLaw.makeTypes(new ArrayList<Expression>());
				body = reverseLaw.getLawBody();
				Expression leftSide = ((Application) body).getChild(0);
				Expression rightSide = ((Application) body).getChild(1);
				List<Variable> rightUnique = ExpressionUtils.getUniqueVars(rightSide, leftSide, reverseLaw.getLawVars());
				List<Variable> leftUnique = ExpressionUtils.getUniqueVars(leftSide, rightSide, reverseLaw.getLawVars());

				//add right side, reference left   (A≥B)
				n = lawMap.size();		
				info = new LawInfo(leftSide, law, leftUnique, lawDir);
				lawMap.put(n, info);
				for (Variable v : reverseLaw.getLawVars()) {
					subst.put(new VarIndex(n,v), null);
				}
				this.add(rightSide, n);

				//add left side, reference right    (B≤A)

				lawClone = reverseLaw.clone();
				reverseLaw.copyTypes(lawClone);
				reverseLaw = lawClone;
				body = reverseLaw.getLawBody();
				leftSide = ((Application) body).getChild(0);
				rightSide = ((Application) body).getChild(1);
				rightUnique = ExpressionUtils.getUniqueVars(rightSide, leftSide, reverseLaw.getLawVars());
				leftUnique = ExpressionUtils.getUniqueVars(leftSide, rightSide, reverseLaw.getLawVars());

				n = lawMap.size();		
				info = new LawInfo(rightSide, null, rightUnique, reverseLaw.getDirection());
				lawMap.put(n, info);
				
				for (Variable v : reverseLaw.getLawVars()) {
					subst.put(new VarIndex(n,v), null);
				}
				
				this.add(leftSide, n);

				//add the law body, referencing ⊤ (B≤A = ⊤)

				lawClone = reverseLaw.clone();
				reverseLaw.copyTypes(lawClone);
				reverseLaw = lawClone;
				body = reverseLaw.getLawBody();
				leftSide = ((Application) body).getChild(0);
				rightSide = ((Application) body).getChild(1);
				rightUnique = ExpressionUtils.getUniqueVars(rightSide, leftSide, reverseLaw.getLawVars());
				leftUnique = ExpressionUtils.getUniqueVars(leftSide, rightSide, reverseLaw.getLawVars());

				n = lawMap.size();			
				info = new LawInfo(top, law);
				lawMap.put(n, info);
				
				for (Variable v : reverseLaw.getLawVars()) {
					subst.put(new VarIndex(n,v), null);
				}
				
				this.add(body,n);				

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * After unification is complete, we are likely to have the maps of instantiables cluttered
	 * with garbage. This could actually cause a future unification to wrongly fail. To prevent
	 * that, we clean our maps. All used instantiables are recorded in 'current' during unification
	 */
	public void clean() {		
		for (VarIndex vi:current.keySet()) {
			if (subst.containsKey(vi)) {
				subst.put(vi, null);
			}
			if (scopeSubst.containsKey(vi)) {
				scopeSubst.put(vi, null);
			}
			if (functionized.containsKey(vi)) {
				functionized.put(vi, null);
			}			
		}
			
		//current = new IdentityHashMap<VarIndex,Expression>();
		current.clear();
	}	
	
	public List<Step> unify(Expression expn, Direction dir) {		
		//Figure out which laws to unify
		List<Integer> success = this.unifyHelper(expn);
		List<Step> steps = new ArrayList<Step>();
		
		//Init the correct values
		Unify.init_unify(this.lawMap, this.subst, this.scopeSubst, this.functionized);
		
		//Prepare the thread pool
		int cores = Runtime.getRuntime().availableProcessors();		
		ExecutorService executor = Executors.newWorkStealingPool(cores);
		Set<Future<Step>> set = new HashSet<Future<Step>>();
		
		//Run each thread
		for (Integer i: success) {
		    set.add(executor.submit(new Unify(i, dir)));
		}
		
		//Get the results and return
		for (Future<Step> fs: set) {			
			try {
				Step s = fs.get();
				
				if (s != null) {
					steps.add(s);
				}
 			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		return steps;
	}
	
	private List<Integer> unifyHelper(Expression expn) {		
		List<Integer> success = new ArrayList<Integer>();
		
		if (expn instanceof Literal) {
			success = litMap.get(expn) == null? success : litMap.get(expn);
		} else if (expn instanceof Variable) {
			success = varMap.get(expn) == null? success : varMap.get(expn);
		} else if (expn instanceof AbbreviatedScope) { // we do not allow Scope instances anymore
			if (children.size() == 2) { //scopes need at least 2 children				
				//look up scope id in scope map
				success = scopeMap.get(expn) == null? success : scopeMap.get(expn);
				
				//unify body and domain, and merge
				success = ExpressionUtils.intersect(success, children.get(0).unifyHelper(((Scope) expn).getDomain()));
				success = ExpressionUtils.intersect(success, children.get(1).unifyHelper(((Scope) expn).getBody()));
			}
		} else if (expn instanceof Application) {
			int arity = ((Application)expn).arity();
			if (children.size() > 0) { //unify operator					
				success = children.get(0).unifyHelper(((Application)expn).getFunction()); 				
			}
			
			//from arity to 0, unify and merge
			if (children.size() >= arity + 1 && !success.isEmpty()) {
				for (int i = arity; i >= 1; i--) {					
					List<Integer> sub = this.children.get(i).unifyHelper(((Application)expn).getChild(i-1));		
					
					success = ExpressionUtils.intersect(success, sub);		
					if(success.isEmpty()) {
						break;
					}
				}
			}
		}
		
		boolean changed = false;
		//take care of instantiables in this node
		for (Variable v:this.instVarMap.keySet()) {
			assert instVarMap.get(v).size() == 1; //instantiables should not belong to more than 1 law
			int n = instVarMap.get(v).get(0);
			VarIndex vi = new VarIndex(n, v);
			
			//map instantiable to the expression
			if(this.locFunMap.containsKey(v)) { //functionize expn
				assert subst.containsKey(vi);
				assert functionized.containsKey(vi);
				assert locVarMap.containsKey(v);
				
				Scope fa = this.functionize(this.locVarMap.get(v), this.locVarMap.get(v).getType(), expn);
				
				if (subst.get(vi)==null || subst.get(vi).equals(fa)) {
					subst.put(vi, fa);
					functionized.put(vi, fa);
					current.put(vi, fa);
					if (!success.contains(n)) {
						success.add(n);
						changed = true;
					}
				}
			} else { //put it in the correct map, if its is not already unified with something else
				Map<VarIndex,Expression> target = (this.subst.containsKey(vi) ? this.subst : this.scopeSubst);
				
				if (target.get(vi)==null || target.get(vi).equals(expn)) {
					target.put(vi, expn);
					current.put(vi, expn);
					if (!success.contains(n)) {
						success.add(n);
						changed = true;
					}
				}
			}
		}
		
		if (changed) {
			Collections.sort(success);
		}
		
		//put expn/functionization in subst/scopeSubst if they do not clash with whats already unified 
		return success;
	}
	
	/**
	 * Go throughout the expression tree and add blob nodes where necessary.
	 * The expression is added to the blob as is: no instantiables are extracted.
	 * This means that on input ∀⟨m:(nat+1)→m>0⟩ the entire expression will be added rather
	 * than m>0. At this point we also assume that the expression is already in the law map. 
	 * 
	 * @param l
	 * @param n Index in lawMap that refers to the expression we should instantiate upon successful unification
	 */
	private void add(Expression law, Integer n) {
		if (law instanceof Literal) {
			this.addToListMap(litMap, (Literal)law, n);
		} else if (law instanceof Variable) {
			VarIndex vi = new VarIndex(n,(Variable)law);
			
			if (instantiable(vi)) {				
				this.addToListMap(instVarMap, (Variable)law, n);
				assert instVarMap.get(law).size() == 1; // exactly 1 law per instantiable
			} else {
				this.addToListMap(varMap, (Variable)law, n);
			}			
		} else if (law instanceof AbbreviatedScope) { // we do not allow Scope instances anymore
			this.addToListMap(scopeMap, ((AbbreviatedScope)law).getId(), n);

			//add the variable to a map of local instantiables
			scopeSubst.put(new VarIndex(n,((AbbreviatedScope)law).getDummy()), null);
			Identifier id = ((AbbreviatedScope)law).getId();
			if (id == Identifiers.var || id == Identifiers.result) {
				
			}
			
			LawBlob b;
			
			if (this.children.size() <= 0) { //add domain
				b = new LawBlob(this);
				children.add(b);				
			} else {
				b = children.get(0);
			}		
			
			b.add(((Scope)law).getDomain(), n);
			
			if (this.children.size() <= 1) { //add body
				b = new LawBlob(this);
				children.add(b);
			} else {
				b = children.get(1);
			}
			
			b.add(((Scope)law).getBody(), n);			
		} else if (law instanceof Application) {
			assert ((Application)law).getFunction() instanceof Variable;
			LawBlob b;			
			
			if (this.children.size() <= 0) { //add operator
				b = new LawBlob(this);
				children.add(b);
			} else {
				b = children.get(0);
			}
			
			Variable v = (Variable) ((Application)law).getFunction();
			b.add(v, n);

			for (int i = 0; i < ((Application)law).arity(); i++) {//add children
				if (this.children.size()<=i+1) { 
					b = new LawBlob(this);
					children.add(b);
				} else {
					b = children.get(i+1);
				}
				Expression e = ((Application)law).getChildren().get(i);
				b.add(e, n);				
			}
			
			if (this.localVarFunction((Application)law, n)) {
				Variable f = (Variable)((Application)law).getArgs()[0];
				VarIndex vi = new VarIndex(n,f);
				Variable loc = (Variable) ((Application)law).getArgs()[1];;
				
				functionized.put(vi, null);
				this.addToListMap(locFunMap, f, n);
				locVarMap.put(f, loc);
			}			
		} else {
			//error
		}
	}
	
	private Scope functionize(Variable var, Expression dom, Expression body) {
		Map<Variable,Expression> m = new IdentityHashMap<Variable,Expression>();
		Variable varClone = (Variable)var.clone();
		m.put(var, varClone);
		body.instantiate(m);
		Scope s = new AbbreviatedScope(Identifiers.scope, varClone, dom, body);
		return s;
	}
	
	private boolean localVarFunction(Application law, int n) {
		return law.is_fun_application() 
		&& law.getArgs()[0] instanceof Variable
		&& law.getArgs()[1] instanceof Variable
		&& subst.containsKey(new VarIndex(n, (Variable)law.getArgs()[0])) //WARNING this might not allow f to be a local variable, but thats not really important
		&& scopeSubst.containsKey(new VarIndex(n,(Variable)law.getArgs()[1]));
	}
	
	private boolean instantiable(VarIndex vi) {
		return subst.containsKey(vi) || scopeSubst.containsKey(vi);
	}
	
	/**
	 * Given a map that maps expressions to a list of integers, we add an expression/int pair
	 * as follows: if the law is already in the map, then we add the input integer to its list
	 * (but only if the list does not already contain it).
	 * 
	 * Otherwise, we create a new list with the input integer, and put the pair in the map.
	 * 
	 * @param <T>
	 * @param m
	 * @param law
	 * @param n
	 */
	private <T> void addToListMap(Map<T, List<Integer>> m, T law, Integer n) {
		if (m.containsKey(law)) {
			List<Integer> l = m.get(law);
			if (!l.contains(n)) {
				l.add(n);
				Collections.sort(m.get(law));
			}
		} else {
			List<Integer> l = new ArrayList<Integer>();
			l.add(n);
			m.put(law, l);
		}
	}
}
