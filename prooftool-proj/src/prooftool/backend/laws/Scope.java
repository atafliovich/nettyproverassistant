package prooftool.backend.laws;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.Substitution;
import prooftool.backend.ccode.ExpressionVisitor;
import prooftool.util.ExpressionUtils;
import prooftool.util.objects.Dictionary;

/**
 * This class represents a scope expression. It is used as the only method of
 * introducing variables, as well as its usual use for functions (which is 
 * really the same thing). 
 * 
 * @author robert
 * @author evm
 *
 */

public abstract class Scope extends Expression {
	private static final long serialVersionUID = -5543766512876524668L;
	
	/**
	 * During parsing, ‘dummy’ can be an Identifier, but after parsing it must be
	 * a Variable.
	 * ‘dummy’ lives and dies with the scope and it is referenced from within ‘body’
	 * but must not be referenced from anywhere else. 
	 */
	protected Expression dummy;	// non-null
	private boolean bunchwise; // == “ ↦ type function that takes its arguments bunch-wise”
	protected Expression domain;
	public Expression body;	// non-null
	protected int cornerIndex = -1;
	
	/**
	 * if there's no domain, just pass null.
	 * @argument 
	 */
	public Scope(Expression dummy, Expression domain, boolean bunchwise, Expression body) {
		this.dummy  = dummy;
		this.bunchwise = bunchwise;		
		this.domain = (domain == null? ExpressionUtils.ALL: domain);
		this.body = body;
	}
	
	/**
	 * Creates a list of expression parts returned are at one zoom level down. 
	 * 
	 * @return A list of String and Object, where the object parts are zoomable
	 *         parts and the String parts are operators/keywords.
	 */
	@Override
	public Object[] toParts()
	{
		String arrow = bunchwise ? "↦" : "→" ;
		if (domain == null) {
			return new Object[] {"⟨", dummy.toString(), arrow, body, "⟩"};
		} else {
			if (domain instanceof Application) {
				return new Object[] {"⟨", dummy.toString(), ":", "(", domain, ")", arrow, body, "⟩"};
			} else {
				return new Object[] {"⟨", dummy.toString(), ":", domain, arrow, body, "⟩"};
			}
		}
	}

	/**
	 * Turn every identifier in this expression into its corresponding variable.
	 * The variable it refers to depends on scope (since an identical 
	 * identifier within a different scope is really a different variable).
	 * 
	 * This method changed all identifiers that refer to the same variable to 
	 * actually be that same variable by making them all the same object.
	 * 
	 * The dummy introduces a new variable, and hence it is placed in the 
	 * dictionary with the identifier. At this point it might displace any 
	 * previous mapping of the same identifier, and this means that we have
	 * the same identifier name in an internal scope. 
	 * 
	 * @param A dictionary containing a mapping of identifiers to variables
	 * @return This expression, with all its identifiers turned into variables
	 */
	@Override
	public Expression makeVariables(Dictionary d) {		
		if (domain != null) {
			domain = domain.makeVariables(d);
		}
		
		Variable dummy_var = new Variable((Identifier) dummy);
		Variable old = d.put(dummy_var);
		body = body.makeVariables(d);
		d.reset((Identifier) dummy, old);
		dummy = dummy_var;
		return this;
	}
	
	@Override
	public void makeTypes(List<Expression> context) {
		// for temp compatibility with domainless scopes
		if (domain == null) {
			dummy.setType(ExpressionUtils.ALL);
		} else {
			domain.makeTypes(context);
			dummy.setType(domain);
		}

		body.makeTypes(context);

		if (body.getType() == null) {
			this.setType(null);
		} else {
			this.setType(ExpressionUtils.parse(dummy.getType() + "→" + body.getType()));
		}	
	}

	/**
	 * Returns whether the given variable x is in this expression (either
	 * dummy, domain or body). Note that it must be the same object in order
	 * to be found.
	 * 
	 * @return Whether x is in this expression
	 */
	@Override
	public boolean contains(Variable x)
	{
		if ( x.getId() == getDummy().getId() )
			return false;
		if ( domain != null )
			if ( domain.contains(x) )
				return true;
		return body.contains(x);
	}
	
	@Override
	public boolean contains(Identifier x) {		
		if ((domain != null) && (domain.contains(x))) {
			return true;
		}
		
		if (x == getDummy().getId()) {
			return false;
		}
		return body.contains(x);
	}

	@Override
	public Expression instantiate(Map<Variable, Expression> subst) {
		if ( domain != null) {
			domain = domain.instantiate(subst);
		}		
		body = body.instantiate(subst);		
		dummy = dummy.instantiate(subst);
		return this;
	}
	
	@Override
	public Expression instantiate(Substitution s) {
		if ( domain != null) {
			domain = domain.instantiate(s);
		}		
		body = body.instantiate(s);		
		dummy = dummy.instantiate(s);
		return this;
	}
	
	@Override
	public Map<Variable,Expression> getAllVars(Map<Variable,Expression> currentVars){
		if (domain != null) {
			currentVars.putAll(domain.getAllVars(currentVars));
		}
		if (dummy instanceof Variable) {
			currentVars.put((Variable)dummy, dummy);
		}
		currentVars.putAll(body.getAllVars(currentVars));		
		return currentVars;
	}
	
	@Override
	public Map<Variable,Variable> getAllVarsHelper(){
		Map<Variable,Variable> vars = new IdentityHashMap<Variable,Variable>();
		if (domain != null) {
			vars.putAll(domain.getAllVarsHelper());
		}
		if (dummy instanceof Variable) {
			vars.put((Variable)dummy, new Variable(((Variable)dummy).getId()));
		}
		vars.putAll(body.getAllVarsHelper());
		return vars;
	}	
	
	@Override
	public void setChild(int i, Expression replacement) {
		if (domain == null) {
			assert (i == 0);
			body = replacement;
		} else {
			assert ((i==0)||(i==1));
			if (i==0) {
				domain = replacement;
			} else if (i==1) {
				body = replacement;
			}
		}		
	}
	
	@Override
	public Expression getChild(int i) {
		if (i==0) {
			return domain;
		} else if (i==1) {
			return body;
		}
		return null;
	}
	
	@Override
	public List<Expression> getChildren() {
		List<Expression> children = new ArrayList<Expression>(2);
		children.add(domain);
		children.add(body);
		return children;
	}
	
	@Override
	public void flatten() {
		if ( domain != null) {
			domain.flatten();
		}		
		 body.flatten();		
	}

	@Override
	public String getTreeRep() {
		return "(scope " + domain.getTreeRep() + " " + body.getTreeRep() + ")";
	}

	@Override
	public <T> T acceptVisitor(ExpressionVisitor<T> visitor) {
		return visitor.visit(this);
	}
	
	// Scopes with several dummies are syntactic sugar which the book
	// uses only when the domain is not explicit. We leave this sugar to the
	// presentation layer and exclude it from the abstract syntax.
	public Variable getDummy() { 
		return (Variable) dummy; 
	}
	
	protected void setDummy(Variable newDummy) {
		this.dummy = newDummy;
	}
	
	public Expression getBody() { 
		return body; 
	}
	
	public void setBody(Expression body) {
		this.body = body;
	}
	
	public Expression getDomain() {
		return this.domain;
	}
	
	protected void setDomain(Expression newDomain) {
		this.domain = newDomain;
	}
}
