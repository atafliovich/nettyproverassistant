package prooftool.backend.suggestions;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.AbbreviatedScope;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Identifier;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;


public class FunctionApplier implements SuggestionGenerator {

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> laws) {
		List<Suggestion> ret = new ArrayList<Suggestion>();
		
		if ((focus instanceof Application) && (((Application) focus).is_fun_application()) && (((Application) focus).getChild(0) instanceof Scope)) {
			Scope sc = (Scope) ((Application) focus).getChild(0).clone();
			Expression argument = ((Application) focus).getChild(1).clone();

			// ensure this is not an abbreviated scope such as var or ivar or let
			if (sc instanceof AbbreviatedScope) {
				Identifier id = ((AbbreviatedScope) sc).getId();
				
				if (!(id == Identifiers.scope || id == Identifiers.bunch_scope || id == Identifiers.procedure_scope)) {
					return ret;
				}
			}

			Map<Variable, Expression> subst = new IdentityHashMap<Variable, Expression>();
			subst.put(sc.getDummy(), argument);

			Direction newDir = ExpressionUtils.eqDir;
			Expression newExpn = sc.getBody().instantiate(subst);

			Justification j = new Justification("Function Application", true);
			ret.add(new Suggestion(new Step(newDir, newExpn), j, this.getGeneratorName()));
		}
		
		return ret;
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, List<SuggestionGenerator> gens) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}
	
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public String getGeneratorName() {
		return "Function Applier";
	}
}
