package prooftool.backend.suggestions;

import java.util.List;

import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.proofrepresentation.suggestion.Suggestion;

public interface SuggestionGenerator {
	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> laws);
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws);
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context);
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, List<SuggestionGenerator> gens);
	
	public String getGeneratorName();
}
