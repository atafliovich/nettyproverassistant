/**
 * 
 */
package prooftool.backend.suggestions;

import java.util.List;
import java.util.ArrayList;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Literal;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;
import prooftool.util.ExpressionUtils;
import prooftool.util.Identifiers;

/**
 * @author dizzy
 *
 */
public class ProbabilityNormalizer implements SuggestionGenerator {

	/* (non-Javadoc)
	 * @see prooftool.backend.SuggestionGenerator#generate(prooftool.backend.Expression, prooftool.backend.Direction, java.util.List)
	 */
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> laws) {
		ArrayList<Suggestion> sug = new ArrayList<Suggestion>();
		ArrayList<Suggestion> ret = new ArrayList<Suggestion>();
		
		sug.add(normalizeSum(focus));
		sug.add(convertDivisionSuggestion(focus));
		
		for (Suggestion s: sug){
			if (s != null) ret.add(s);
		}
		return ret;
	}
	
	private Suggestion normalizeSum(Expression focus) {
		if (focus instanceof Application && ((Application)focus).getFunId().equals(Identifiers.sum)){
			Expression focusClone = bringToFrontRecursor(focus, new ArrayList<Variable>());

			try {
				if (!focus.equals(focusClone)){
					Direction newDir = ExpressionUtils.eqDir;	            			
					Justification j = new Justification("Normalize for One Point", true);
					
					return new Suggestion(new Step(newDir, focusClone), j, this.getGeneratorName());
				}
			} catch (NullPointerException e) {
			}
		}
		
		return null;
	}
	
	private Expression bringToFrontRecursor(Expression focus, List<Variable> vars){
		if (focus instanceof Application && ((Application)focus).getFunId().equals(Identifiers.sum)){
			Expression focusClone = focus.clone();
			Scope sc = ((Scope)((Application)focusClone).getChild(0));
			vars.add(sc.getDummy());
			
			if (sc.getBody() instanceof Application && ((Application)sc.getBody()).getFunId().equals(Identifiers.sum)) {
				sc.setBody(bringToFrontRecursor(sc.getBody(), vars));
			} else {
				for (Variable var: vars){
					sc.setBody(bringToFront(sc.getBody(), var));
				}
				
				sc.setBody(rightAssociate(sc.getBody()));
			}
			
			focusClone.setChild(0, sc);
			return focusClone;
		}
		return null;
	}
	
	private Expression rightAssociate(Expression body) {
		if (body instanceof Application){
			String ret = "";
			Application bodyClone = (Application)body.clone();
			String fun = bodyClone.getFunction().toString();
			bodyClone.flatten();
			
			for (Expression child: bodyClone.getChildren()) {
				ret += "((" + child.toString() + ")" + fun;
			}
			
			if (ret.length() != 0) {
				ret = ret.substring(0, ret.length() - fun.length());
			}
			
			for (int i = 0; i < bodyClone.arity(); i++) {
				ret += ")";
			}
			
			return ExpressionUtils.parse(ret);
		}
		
		return body;
	}

	private Expression bringToFront(Expression focus, Variable var){
		if (focus instanceof Application) {
			if (((Application)focus).getFunId().equals(Identifiers.times)) {
				String prefix = "";
				String ret = "";
				String suffix = "";
				boolean changed = false;
				focus = focus.clone();
				focus.flatten();
				for (Expression child:focus.getChildren()){
					if (child instanceof Application 
							&& !changed
							&& ((Application)child).getFunId().equals(Identifiers.eq)
							&& (child.getChild(0).equals(var)||child.getChild(1).equals(var))
							) {
						if (child.getChild(0).equals(var)) {
							prefix += "(" + child.toString() + ")×";
						} else {
							prefix += "(" + child.getChild(1).toString() + "=" + child.getChild(0).toString() + ")×";
						}
						changed = true;
					} else if (child instanceof Application 
							&& ((Application)child).getFunId().equals(Identifiers.divide)
							&& child.getChild(0) instanceof Literal
							&& child.getChild(1) instanceof Literal) {
						suffix += "(" + child.toString() + ")×";
					} else {
						ret += "(" + child.toString() + ")×";
					}
				}
				if (!changed) {
					prefix = "";
					ret = "";
					for (Expression child:focus.getChildren()){
						if (child instanceof Application 
								&& !changed
								&& (((Application)child).getFunId().equals(Identifiers.lte)
										|| ((Application)child).getFunId().equals(Identifiers.gte)
										|| ((Application)child).getFunId().equals(Identifiers.gt)
										|| ((Application)child).getFunId().equals(Identifiers.lt))
								&& (child.getChild(0).equals(var)||child.getChild(1).equals(var))
								) {
							if (child.getChild(0).equals(var)) {
								prefix += "(" + child.toString() + ")×";
							} else {
								prefix += "(" + child.getChild(1).toString() + ((Application)child).getFunction().toString() + child.getChild(0).toString() + ")×";
							}
							changed = true;
						} else {
							ret += "(" + child.toString() + ")×";
						}
					}
				}
				
				ret = prefix + ret + suffix;
				if (!ret.equals("")) ret = ret.substring(0, ret.length() - 1);
				return ExpressionUtils.parse(ret);
			}
		}
		return focus;
	}
	
	private Suggestion convertDivisionSuggestion(Expression expression) {
		Expression expr = ExpressionUtils.parse(convertDivision(expression).toString());
		if (!expr.equals(expression)){
			Direction newDir = ExpressionUtils.eqDir;
			Justification j = new Justification("Normalize Fractions", true);
			return new Suggestion(new Step(newDir, expr), j, this.getGeneratorName());
		} else {
			return null;
		}
	}
	
	private Expression convertDivision(Expression expression) {
		if (expression instanceof Application){
			if (((Application)expression).getFunId().equals(Identifiers.divide)) {
				Expression left = expression.getChild(0);
				Expression right = expression.getChild(1);
				final Literal one = new Literal(1);
				if (!left.equals(one)) {
					if (!right.equals(one)) {
						right = new Application(Identifiers.divide, one, right);
						return new Application(Identifiers.times, left, right);
					} else {
						return left;
					}
				} else {
					if (!right.equals(one)) {
						return new Application(Identifiers.divide, one, right);
					} else {
						return one;
					}
				}
			} else {
				boolean changed = false;
				Expression expression2 = expression.clone();
				expression2.flatten();
				for (int i = 0; i < ((Application)expression2).arity(); i++) {
					Expression child = convertDivision(expression2.getChild(i));
					if (!child.equals(expression2.getChild(i))) {
						expression2.setChild(i, child);
						changed = true;
					}
				}
				if (changed){
					return expression2;
				} else {
					return expression;
				}
			}
		} else if (expression instanceof Scope) {
			Scope sc = (Scope)expression.clone();
			sc.setBody(convertDivision(sc.getBody()));
			return sc;
		}
		return expression;
	}
	
	/* (non-Javadoc)
	 * @see prooftool.backend.SuggestionGenerator#generate(prooftool.backend.Expression, prooftool.backend.Direction, prooftool.backend.LawBlob, java.util.List)
	 */
	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, List<SuggestionGenerator> gens) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {
		return generate(focus, requiredDir, laws.getInputLaws());
	}

	@Override
	public String getGeneratorName() {
		return "Probability Normalizer";
	}

}
