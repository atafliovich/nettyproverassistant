package prooftool.backend.suggestions.heuristic;

import java.io.InputStream;
import java.util.List;

import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.util.Main;

public class Heuristic {

	private static InputStream heuURL;

	private LawBlob laws;
	
	protected Heuristic () {
		try {
			//Get resources
			InputStream stream = Heuristic.class.getResourceAsStream("/resources/heuristic");
			Heuristic.heuURL = stream;
			
			this.laws = new LawBlob();
			
			List<Expression> laws = Main.parse(Heuristic.heuURL);
			
			for (Expression law: laws) {
				this.laws.addHeuristic(law);
			}
		} catch (Exception e) {
			System.err.println("Error in reading heuristic laws");
			e.printStackTrace();
		}			
	}	
	
	protected LawBlob getLaws() {
		return this.laws;
	}
}
