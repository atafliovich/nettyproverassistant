package prooftool.backend.suggestions;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.Step;
import prooftool.backend.direction.Direction;
import prooftool.backend.lawblob.LawBlob;
import prooftool.backend.laws.Expression;
import prooftool.proofrepresentation.justification.Justification;
import prooftool.proofrepresentation.suggestion.Suggestion;

public class UnifyingSuggestor implements SuggestionGenerator {

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, List<Expression> laws) {
		return new ArrayList<Suggestion>();
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws) {
		List<Step> steps = laws.unify(focus, requiredDir);
		laws.clean();		
		
		List<Suggestion> s = new ArrayList<Suggestion>();
		for (Step step : steps) { 			
			Justification j = new Justification(step.getLaw());
			s.add(new Suggestion(step, j, this.getGeneratorName()));                 
		}
		
		return s;
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, List<SuggestionGenerator> gens) {		
		return this.generate(focus, requiredDir, laws);
	}

	@Override
	public List<Suggestion> generate(Expression focus, Direction requiredDir, LawBlob laws, LawBlob context) {		
		List<Step> steps0 = laws.unify(focus, requiredDir);
		List<Step> steps1 = context.unify(focus, requiredDir);
		laws.clean();	
		context.clean();
		
		List<Suggestion> s = new ArrayList<Suggestion>();
		
		for (Step step : steps0) { 			
			Justification j = new Justification(step.getLaw());
			s.add(new Suggestion(step, j, this.getGeneratorName()));   
		}
		
		for (Step step : steps1) {             
			Justification j = new Justification(step.getLaw());
			s.add(new Suggestion(step, j, this.getGeneratorName()));                 
		}
		
		return s;
	}

	@Override
	public String getGeneratorName() {
		return "Unifying Suggestor";
	}
}
