package prooftool.backend.direction;

import java.io.Serializable;

import prooftool.backend.laws.Identifier;
import prooftool.util.Identifiers;
import prooftool.util.Symbol;
import prooftool.util.objects.DirectionInfo;

/**
 * 
 * @author evm
 *
 */

public class ExpnDirection implements Direction, Serializable{
	private static final long serialVersionUID = 2041636983973036576L;
	private String direction;
	private Identifier id;
	
	public ExpnDirection(String dir) {
		this.direction = Symbol.lookupUnicode(dir);//recent change
		String uniDir = Symbol.lookupUnicode(dir);
		
		if (uniDir.equals(Identifiers.imp.toString()) || uniDir.equals(Identifiers.rev_imp.toString())) {
			this.id = Identifiers.get_infix(11, uniDir);
		} else {
			this.id = Identifiers.get_infix(7, uniDir);
		}
	}
	
	public ExpnDirection(Identifier id) {
		this.id = id;
		this.direction = id.toString();
	}
	
	/**
	 *
	 */
	public boolean isCompatible(Direction anotherDirection) {
		DirectionInfo di = Symbol.operatorMap.get(direction).getDirInfo();
		if ((di != null)&&(anotherDirection!=null)) {
			for (String dir: di.getCompatibleConnectives()) {
				if (anotherDirection.toString().equals(dir)){
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Return the reverse of this direction
	 */
	public Direction reverse() {		
		return new ExpnDirection(Symbol.reverseConnectives.get(id));
	}
	
	public String toString() {
		if (this.direction == null) {
			return "";
		} else {
			return this.direction;
		}
	}

	public Identifier getIdent() {
		return id;
	}
	
	@Override
	public boolean equals(Direction other) {		
		return this.toString().equals(other.toString());
	}
}
