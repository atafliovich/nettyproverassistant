package prooftool.backend.refinement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prooftool.backend.ParameterizedExpression;
import prooftool.backend.ParameterizedRefinement;
import prooftool.backend.Substitution;
import prooftool.backend.laws.Application;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Scope;
import prooftool.backend.laws.Variable;
import prooftool.util.ListUtils;

public class ContextRefinementProvider implements RefinementProvider {
	private static class ParametricRefinement {
		public ParametricRefinement(Expression expression, Expression refinement, List<Variable> instantiables)
		{
			this.expression = expression;
			this.refinement = refinement;
			this.instantiables = instantiables;
		}
		
		public Expression expression;
		public Expression refinement;
		public List<Variable> instantiables;
	}
	
	private Map<Expression, List<Expression>> nonparametricRefinements = new HashMap<Expression, List<Expression>>();
	private List<ParametricRefinement> parametricRefinements = new ArrayList<ParametricRefinement>();
	
	public ContextRefinementProvider(List<Expression> laws) {
		for (Expression law : laws) {
			System.out.println("Consider law: " + law.toAsciiString());
			ParameterizedExpression pExpn = unwrapUniversals(law);
			Expression unwrappedExpn = pExpn.getExpression();
			System.out.println("unwarpped law is: " + pExpn.getExpression().toAsciiString());
			List<Variable> instantiables = pExpn.getParameters();
			if (unwrappedExpn instanceof Application)
			{
				System.out.println("law is instance of application");
				Application app = (Application)unwrappedExpn;
				Expression funExpn = app.getFunction();
				System.out.println("function expression is: " + funExpn.toAsciiString());
				if (funExpn instanceof Variable)
				{
					Variable fun = (Variable)funExpn;
					if (fun.getId().toString().equals("="))
					{
						assert app.getArgs().length == 2;
						addRefinement(app.getArgs()[0], app.getArgs()[1], instantiables);
						addRefinement(app.getArgs()[1], app.getArgs()[0], instantiables);
					}
					else if (fun.getId().toString().equals("⇒"))
					{
						assert app.getArgs().length == 2;
						addRefinement(app.getArgs()[1], app.getArgs()[0], instantiables);
					}
					else if (fun.getId().toString().equals("⇐"))
					{
						assert app.getArgs().length == 2;
						addRefinement(app.getArgs()[0], app.getArgs()[1], instantiables);
					}
				}
			}
		}
		System.out.println("Have non-parametric refinements for " + nonparametricRefinements.size() + " expressions");
		System.out.println("Have " + parametricRefinements.size() + " parametric refinements");
	}
	
	@Override
	public List<ParameterizedRefinement> getRefinementsOf(Expression e)
	{
		List<ParameterizedRefinement> result = new ArrayList<ParameterizedRefinement>();
		if (nonparametricRefinements.containsKey(e))
		{
			for (Expression refinement : nonparametricRefinements.get(e))
				result.add(new ParameterizedRefinement(e, refinement, new Substitution(new ArrayList<Variable>())));
		}
		else
		{
			for (ParametricRefinement pref : parametricRefinements)
			{
//				System.out.println("considering parametric refinement where");
//				System.out.println("  expression is: " + pref.expression.toAsciiString());
//				System.out.println("  refinement is: " + pref.refinement.toAsciiString());
//				System.out.println("   unifying with: " + e.toAsciiString());
				Substitution subst = pref.expression.unify_with(e.clone(), pref.instantiables);
				if (subst != null)
				{
					boolean allExprsHaveSubs = true; 
					for (Variable key : subst.getSubst().keySet())
					{
						if (subst.getSubst().get(key) == null)
							allExprsHaveSubs = false;
					}
					if (allExprsHaveSubs)
					{
						System.out.println("   unification succeeded!");
						result.add(new ParameterizedRefinement(pref.expression, pref.refinement, subst));
					}
				}
			}
		}
		return result;
	}
	
	// Helper functions
	
	private void addRefinement(Expression expression, Expression refinement, List<Variable> instantiables)
	{
		// Ignore refinement if the expression contains no non-instantiable variables.
		// (Otherwise we'd be led astray by refinement such as "a <-- a \/ a" where the left side
		// unifies with everything, but the refinement doesn't get us anywhere.)
		List<Variable> allVars = expression.getAllVars();
		boolean hasNoninstantiables = false;
		for (Variable var : allVars)
			if (!instantiables.contains(var))
				hasNoninstantiables = true;
		if (!hasNoninstantiables)
			return;
		
		if (instantiables.isEmpty())
			ListUtils.listMapPut(nonparametricRefinements, expression, refinement);
		else
			parametricRefinements.add(new ParametricRefinement(expression, refinement, instantiables));
	}
	
	// Unwrap every top-level universal from 'e', putting the corresponding variables
	// (with their correct types!) into the ParameterizedExpression's parameter list 
	private ParameterizedExpression unwrapUniversals(Expression e)
	{
		if (e instanceof Application)
		{
			Application app = (Application)e;
			Expression function = app.getFunction();
			if (function instanceof Variable)
			{
				Variable vFunction = (Variable)function;
				if (vFunction.getId().toAsciiString().equals("\\forall"))  // TODO: string comparison
				{
					assert app.getArgs().length == 1;
					Expression arg = app.getArgs()[0];
					if (arg instanceof Scope)
					{
						Scope s = (Scope) arg;
						ParameterizedExpression result = unwrapUniversals(s.getBody());
						Variable var = s.getDummy();
						var.setType(s.getDomain());  // important: record type of variable
						result.getParameters().add(var);
						return result;
					}
				}
			}
		}
		return new ParameterizedExpression(e, new ArrayList<Variable>());
	}
}
