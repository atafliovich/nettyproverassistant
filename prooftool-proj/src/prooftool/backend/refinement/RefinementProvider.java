package prooftool.backend.refinement;

import java.util.List;

import prooftool.backend.ParameterizedRefinement;
import prooftool.backend.laws.Expression;

public interface RefinementProvider {
	List<ParameterizedRefinement> getRefinementsOf(Expression e);
}
