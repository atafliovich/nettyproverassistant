package prooftool.backend.refinement;

import java.util.ArrayList;
import java.util.List;

import prooftool.backend.ParameterizedRefinement;
import prooftool.backend.Substitution;
import prooftool.backend.laws.Expression;
import prooftool.backend.laws.Variable;
import prooftool.proofrepresentation.Proof;

public class SubproofRefinementProvider implements RefinementProvider
{
	private Proof proof; 
	
	public SubproofRefinementProvider(Proof proof)
	{
		this.proof = proof;
	}
	
	@Override
	public List<ParameterizedRefinement> getRefinementsOf(Expression e)
	{
		List<ParameterizedRefinement> result = new ArrayList<ParameterizedRefinement>();
		Expression refinement = proof.getRefinement(e);
		if (refinement != null)
			result.add(new ParameterizedRefinement(e, refinement, new Substitution(new ArrayList<Variable>() /* no parameters */)));
		return result;
	}
}
