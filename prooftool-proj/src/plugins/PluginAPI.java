/**
 * 
 */
package plugins;

import java.util.List;

import javax.swing.JComponent;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import prooftool.backend.laws.Expression;
import prooftool.gui.prooftree.ProofLineComponent;
import prooftool.gui.prooftree.ProofTree;
import prooftool.proofrepresentation.Proof;
import prooftool.proofrepresentation.ProofElement;
import prooftool.proofrepresentation.ProofLine;

/**
 * @author ben
 * This class is meant to be used by people making plugins for netty.
 * This class is also completely static.
 *
 */
public class PluginAPI {
	
	/**
	 * The full prooftree
	 */
	public static ProofTree prooftree;
	/**
	 * The text box that the user is typing into
	 */
	public static RSyntaxTextArea textfield;
	private static List<ProofElement> children;
	
	/**
	 * 
	 * @return the proof corresponding to the main prooftree
	 */
	public static Proof getProof() {
		return prooftree.getProof();
	}
	
	/**
	 * This function will update the last two lines of the proof that
	 * are stored within this class.
	 * As such, this function should always be called before getLastLine(),
	 * getSecondLastLine(), and getNumLines()
	 */
	public static void loadLines() {
		// We need to find the two most recent lines, at the current zoom level.
		// We do this as follows: if the most recent child is a Proof, zoom in.
		// Repeat until we find a proof whose most recent child is not a proof.
		// (Instead it is expected to be an input box.) If the two children before
		// that are both ProofLines, we can continue.
		Proof currentProof;
		List<ProofElement> children;
		int size;
		ProofElement lastChild = getProof();
		do {
			currentProof = (Proof)lastChild;
			children = currentProof.getChildren();
			size = children.size();
			assert size > 0;
			lastChild = children.get(size - 1);
		} while (lastChild instanceof Proof);

		PluginAPI.children = children;
		
	}
	
	/**
	 * 
	 * @return the current last line of the proof as a ProofElement
	 */
	public static ProofElement getLastLine() {
		return children.get(children.size() - 2);
		
	}
	
	/**
	 * 
	 * @return the current second last line of the proof as a ProofElement
	 */
	public static ProofElement getSecondLastLine() {
		return children.get(children.size() - 3);
	}
	
	/**
	 * 
	 * @return the number of lines currently in the proof
	 */
	public static int getNumLines() {
		if (children == null) {
			return 0;
		}
		return children.size();
	}
	
	/**
	 * 
	 * @return the "context" of the current proof
	 */
	public static List<List<Expression>> getFullContext() {
		return  getProof().getFullContext();
	}
	
	/**
	 * Refreshes the two last lines if any changes were made.
	 */
	public static void refreshLastLines() {
		ProofElement lastLine = PluginAPI.getLastLine();
		ProofElement secondLastLine = PluginAPI.getSecondLastLine();
		
		JComponent c = ((ProofLine)secondLastLine).getPanel();
		((ProofLineComponent)c).refresh();
		c = ((ProofLine)lastLine).getPanel();
		((ProofLineComponent)c).refresh();
	}

}
