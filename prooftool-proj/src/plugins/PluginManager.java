/**
 * 
 */
package plugins;

import java.io.File;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.net.URL;
import javax.swing.Icon;

import prooftool.gui.Settings;
import prooftool.gui.UIBits;

/**
 * @author ben
 *
 */
public class PluginManager {
	
	private static PluginManager instance = new PluginManager();
	public HashMap<Integer, NettyPlugin> ui_numbers = new HashMap<>();
	public ArrayList<NettyPlugin> plugins = new ArrayList<>();
	
	
	public static PluginManager getInstance() {
		return instance;
	}
	
	private PluginManager() {
		loadPlugins(Settings.get_Settings().getPlugins());
	}

	private  void loadPlugins(List<String> files)  {
		try {
			//the number of buttons in uibits that already exist
			Integer i = UIBits.NewTabToolBarTextIcons.length;
			for (String file: files) {
				File jarplugin = new File(file);
				ClassLoader raw_plugin = URLClassLoader.newInstance(new URL[] { jarplugin.toURI().toURL() } );
				int pos = file.lastIndexOf(".");
				if (pos > 0) {
					String name = file.substring(0, pos);
					NettyPlugin plugin = (NettyPlugin) raw_plugin.loadClass(name + "." + "MainNetty").newInstance();
					this.ui_numbers.put(i, plugin);
					this.plugins.add(plugin);
					i++;
				}
			}
		} catch (Exception e) {
			System.out.println("Failed to load plugins!");
			e.printStackTrace();
		}

	}
	
	
	public Icon[] getImageFiles() {
		Icon[] result = new Icon[this.plugins.size() + UIBits.NewTabToolBarImageFiles.length];
		int i = 0;
		for (Icon icon: UIBits.NewTabToolBarImageFiles) {
			result[i] = icon;
			i++;
		}
		for (NettyPlugin plug: this.plugins) {
			result[i] = plug.getIcon();
			i++;
		}
		return result;
	}
	
	public String[] getTextIcons() {
		String[] result = new String[this.plugins.size() + UIBits.NewTabToolBarTextIcons.length];
		int i = 0;
		for (String icon: UIBits.NewTabToolBarTextIcons) {
			result[i] = icon;
			i++;
		}
		for (NettyPlugin plug: this.plugins) {
			result[i] = plug.getPluginName() ;
			i++;
		}
		return result;
	}
	
	public String[] getToolTips() {
		String[] result = new String[this.plugins.size() + UIBits.NewTabToolBarToolTips.length];
		int i = 0;
		for (String tip : UIBits.NewTabToolBarToolTips) {
			result[i] = tip;
			i++;
		}
		for (NettyPlugin plug: this.plugins) {
			result[i] = plug.getDescription() ;
			i++;
		}
		return result;
	}
	
}
