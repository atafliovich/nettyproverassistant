/**
 * 
 */
package plugins;

import javax.swing.Icon;


/**
 * @author ben
 *
 */
public interface NettyPlugin {
	public void run();
	public String getPluginName();
	public Icon getIcon();
	public String getDescription();
}
