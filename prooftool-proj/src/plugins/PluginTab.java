/**
 * 
 */
package plugins;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Files;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import prooftool.gui.Settings;
import prooftool.gui.UIBits;
import prooftool.gui.elements.NettyFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ben
 *
 */
public class PluginTab extends JPanel implements ActionListener, ListSelectionListener{
	
	private JList<NettyFile> fileList;
	private DefaultListModel<NettyFile> fileListModel;


	/**
	 * 
	 */
	private static final long serialVersionUID = 4113502057561104211L;
	
	public PluginTab() {
		this.setLayout(new BorderLayout());
		
		this.fileListModel = new DefaultListModel<NettyFile>();
		this.fileList = new JList<NettyFile>(this.fileListModel);
		this.fileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.fileList.addListSelectionListener(this);
		initPluginList();
		JScrollPane fileScroll = new JScrollPane();
		fileScroll.setViewportView(this.fileList);
		this.add(fileScroll, BorderLayout.CENTER);
		
		//buttons panel
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(1, 2));
		
		JButton add = new JButton(UIBits.pluginAdd);
		add.setActionCommand(UIBits.pluginAdd);
		add.addActionListener(this);
		
		JButton remove = new JButton(UIBits.pluginRemove);
		remove.setActionCommand(UIBits.pluginRemove);
		remove.addActionListener(this);
		
		buttons.add(add);
		buttons.add(remove);
		
		this.add(buttons, BorderLayout.PAGE_END);
	}
	
	public void initPluginList() {
		List<String> plugins = Settings.get_Settings().getPlugins();
		for (String plugin : plugins) {
			File loaded = new File(plugin);
			NettyFile nettyfile = new NettyFile(loaded);
			this.fileListModel.addElement(nettyfile);
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//We need to remove and selectionlistener to prevent switching
		//while stuff is happening
		this.fileList.removeListSelectionListener(this);
		String action = e.getActionCommand();
		
		//adding a new plugin
		if (action.equalsIgnoreCase(UIBits.pluginAdd)) {
			addPlugin();
			savePlugins();
		} else if (action.equalsIgnoreCase(UIBits.pluginRemove)) {
			removePlugin();
			savePlugins();
		}
		
	}
	
	public void addPlugin() {
		// open a file choose
		JFileChooser fc = new JFileChooser(".");
		
		int returnVal = fc.showDialog(this, "Add");

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File source = fc.getSelectedFile();
			File dest = new File(source.getName());
			
			//here we copy the file to Netty's directory
			try {
				Files.copy(source.toPath(), dest.toPath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		
			this.fileListModel.addElement(new NettyFile(dest));
			this.fileList.setSelectedValue(dest, true);
			JOptionPane.showMessageDialog(this, "A restart is required for the plugins to load.");
		}
	}
	
	private void removePlugin() {
		File selected = this.fileList.getSelectedValue().getFile();
		int selectedIndex = this.fileList.getSelectedIndex();
		
		if (selected == null) {
			return;
		}

		this.fileListModel.remove(selectedIndex);
	}
	
	private void savePlugins() {
		ArrayList<File> newList = new ArrayList<File>();
		for (int i = 0; i < this.fileListModel.getSize(); i++) {
			newList.add(this.fileListModel.getElementAt(i).getFile());
		}
		
		Settings.get_Settings().setPlugins((newList));
		Settings.save_Settings();
	}

}
